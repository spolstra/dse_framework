import pickle
import argparse
import itertools
import matplotlib.pyplot as plt
import pylab

# Colors to plot data sets
colors = ['lightblue', 'blue', 'yellow', 'green']
next_color = itertools.cycle(colors).next

def plot_pareto(data, label, nograph):
    # Setup plot.
    if nograph:
        plt.switch_backend('agg')

    # First data set is taken as pareto. Save it to plot later.
    pareto_front = data.pop(0)

    # Plot the other data sets.
    for d in data:
        x, y = zip(*d)
        try:
            plt.scatter(x, y, s=30, facecolors=next_color(), edgecolors='black')
        except:
            print "Error opening plot window, try using the --nograph option"
            return

    # Plot pareto front (can overwrite other points)
    x, y = zip(*pareto_front)
    print "%s" % pareto_front

    try:
        plt.scatter(x, y, s=60, facecolors='red', edgecolors='black')
    except:
        print "Error opening plot window, try using the --nograph option"
        return

    plt.xlabel('Execution time (Cycles)')
    plt.ylabel(label)
    pylab.savefig('pareto-front.png')
    if (nograph== False):
        plt.show()

def main():
    argparser = argparse.ArgumentParser(description="Pareto Printer")
    argparser.add_argument('data_sets',  nargs='+', help='Data sets to print')
    argparser.add_argument("--energy", "-e", action='store_true',
                           default=False, help="Energy as second objective")
    argparser.add_argument("--nograph", "-n", action='store_true',
                           default=False, help="Don't open plot window")
    args = argparser.parse_args()

    data = []
    # Read data sets.
    for data_set in args.data_sets:
        with open(data_set) as f:
            data.append(pickle.load(f))

    # Print it.
    label = 'Energy' if args.energy else 'Cost'
    plot_pareto(data, label, args.nograph)
