import pickle
import argparse
import itertools
import matplotlib.pyplot as plt
import pylab


def plot_data(data, label):
    # Plot all data sets.
    fig, ax = plt.subplots()
    for (f, d) in data:
        x, y = zip(*d)
        ax.scatter(x, y, s=40, marker=next_marker(),
                    facecolors=next_color(), edgecolors='black', label=f)

    ax.set_xlabel('Execution time (Cycles)')
    ax.set_ylabel(label)
    ax.legend()
    pylab.savefig('data.png')
    plt.show()

def main():
    argparser = argparse.ArgumentParser(description="Pareto Printer")
    argparser.add_argument('data_sets',  nargs='+',
            help='Data sets to print')
    argparser.add_argument("--energy", "-e", action='store_true',
                           default=False, help="Energy as second objective")
    argparser.add_argument('--markers', '-m', nargs='+',
            default=['o', 'v', '^', '<', '>', '*', 's'],
            help='Plot markers')
    argparser.add_argument('--colors', '-c', nargs='+',
            default=['lightblue', 'blue', 'yellow', 'green'],
            help='Marker colors')

    args = argparser.parse_args()
    global next_marker
    next_marker = itertools.cycle(args.markers).__next__
    global next_color
    next_color = itertools.cycle(args.colors).__next__

    data = []
    # Read data sets.
    for data_set in args.data_sets:
        with open(data_set, 'rb') as f:
            data.append((data_set, pickle.load(f)))

    # Print it.
    plot_data(data, 'Energy' if args.energy else 'Cost')
