import pickle
import string
import argparse
import matplotlib.pyplot as plt
import pylab

def plot_perf_cost(logbook, label2, nograph):
    """Plot best performance and cost per generation"""

    # Setup plot.
    if nograph:
        plt.switch_backend('agg')
    try:
        fig, ax1 = plt.subplots()
    except:
        print "Error opening plot window, try using the --nograph option"
        return

    gen, minval = logbook.select("gen", "min")
    time_mins = [ e[0] for e in minval ]
    cost_mins = [ e[1] for e in minval ]

    line1 = ax1.plot(gen, time_mins, "b-", label="Performance")
    ax1.set_xlabel("Generation")
    ax1.set_ylabel("Cycles", color="b")
    for tl in ax1.get_yticklabels():
        tl.set_color("b")

    ax2 = ax1.twinx()
    line2 = ax2.plot(gen, cost_mins, "r-", label=label2)
    ax2.set_ylabel(label2, color="r")
    for tl in ax2.get_yticklabels():
        tl.set_color("r")

    lns = line1 + line2
    labs = [l.get_label() for l in lns]
    ax1.legend(lns, labs, loc="center right")

    pylab.savefig('perf-%s.png' % string.lower(label2))
    if (nograph == False):
        plt.show()


def main():
    argparser = argparse.ArgumentParser(description="LogBook Reader")
    argparser.add_argument("logbook", help="LogBook data",
                           nargs='?', default="logbook.data")
    argparser.add_argument("--nograph", "-n", action='store_true',
                           default=False, help="Don't show plot window.")
    argparser.add_argument("--energy", "-e", action='store_true',
                           default=False, help="Set energy as second objective")
    argparser.add_argument("--balance", "-b", action='store_true',
                           default=False, help="Set balance as second objective")
    args = argparser.parse_args()

    # TODO labels should be pickled by DSE
    if args.energy:
        label2 = 'Energy'
    elif args.balance:
        label2 = 'Balance'
    else:
        label2 = 'Cost'

    # Open logbook.
    with open(args.logbook) as f:
        logbook = pickle.load(f)

    # Print it.
    print logbook
    plot_perf_cost(logbook, label2, args.nograph)
