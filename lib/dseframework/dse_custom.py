import sys
import random
import logging
import argparse

import dsehelpers
from deap import base
from deap import creator
from deap import tools
import matplotlib.pyplot as plt

# Population size.
POPN = 20

"""Main function: Performs the Design Space Exploration."""
def main():
    # Random seed printed to repeat DSE run if needed.
    seed = random.randint(0, sys.maxint)
    logging.info("Using random seed:%d" % seed)
    random.seed(seed)

    # Create population, parento front.
    pop = toolbox.population(n=POPN)
    hof = tools.ParetoFront()

    # Set crossover, mutate and number of generations
    CXPB, MUTPB, NGEN = 0.5, 0.2, 4

    # Initialize project: clean, compile and create application traces
    logging.info("Building project...")
    project.clean()
    project.build_all()
    project.trace()

    # Repair any invalid mappings created during the random initialisation.
    for ind in pop:
        toolbox.mutate(ind)

    # Evaluate all individuals in the population
    logging.info("Evaluating initial population")
    fitnesses = map(toolbox.evaluate, pop)
    for ind, fit in zip(pop, fitnesses):
        ind.fitness.values = fit
        logging.debug("ind:%s fitness:%s" % (ind, fit))

    logging.info("Evaluated %i individuals" % len(pop))

    # Begin design space exploration.
    for g in range(NGEN):
        logging.info("-- Generation %i --" % g)

        # Select the next generation individuals.
        offspring = toolbox.select(pop, len(pop))
        offspring = map(toolbox.clone, offspring)

        # Crossovers, invalidate fitness if the individuals changed.
        for child1, child2 in zip(offspring[::2], offspring[1::2]):
            if random.random() < CXPB:
                toolbox.mate(child1, child2)
                del child1.fitness.values
                del child2.fitness.values

        # Mutations, invalidate fitness if the individual changed.
        for mutant in offspring:
            if random.random() < MUTPB:
                toolbox.mutate(mutant)
                del mutant.fitness.values

        logging.debug("Offspring after mutations: %s" % offspring)

        # Evaluate the individuals that have changed.
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        logging.info("Evaluated %i individuals" % len(invalid_ind))

        # Replace population with offspring.
        pop[:] = offspring

        # Calculate and print statistics of this generation.
        fits = [ind.fitness.values[0] for ind in pop]
        length = len(pop)
        mean = sum(fits) / length
        sum2 = sum(x*x for x in fits)
        std = abs(sum2 / length - mean**2)**0.5

        logging.info("  Min %s" % min(fits))
        logging.info("  Max %s" % max(fits))
        logging.info("  Avg %s" % mean)
        logging.info("  Std %s" % std)
        # Update the non-dominated Pareto front set.
        hof.update(pop)

    logging.info("-- End of (succesful) evolution --")

    # And print the resulting Pareto set of non-dominated individuals.
    print "Pareto set:"
    print hof
    par = []
    for e in hof:
        print e
        print e.fitness.values
        par.append(e.fitness.values)
        
    print par
    # Create the plot
    x, y = zip(*par)
    plt.scatter(x, y, s = 80, facecolors='red', edgecolors='black')
    plt.xlabel('Execution time (Cycles)')
    plt.ylabel('Cost')
    plt.show()

    # End of DSE


# Perform all the initialisation and then call main() for the DSE.
if __name__ == "__main__":

    # Handle arguments
    argparser = argparse.ArgumentParser(description="Design Space Explorer")
    argparser.add_argument("project", help = "Sesame project file")
    argparser.add_argument("-l", "--log", help="Set logging level")
    args = argparser.parse_args()

    # Set logging level.
    if args.log:
        log_level = getattr(logging, args.log.upper(), None)
        if not isinstance(log_level, int):
            raise ValueError('Invalid log level:%s' % args.log)
        logging.basicConfig(level=log_level)
    else:
        logging.basicConfig(level=logging.INFO)

    # Read project data.
    (app, arch, project) = dsehelpers.readProject(args.project)

    # Below is all the code to initialize the DEAP
    # See: http://code.google.com/p/deap/

    # Minimize both execution time and cost.
    creator.create("FitnessMin", base.Fitness, weights=(-1.0,-1.0))
    creator.create("Individual", list, fitness=creator.FitnessMin)

    # Setup the toolbox with helper functions.
    toolbox = base.Toolbox()

    # An individual is initialised as a random sequence. Sequences that
    # are invalid mappings are repaired before they are evaluated.
    toolbox.register("attr_arch", random.randint, 0, len(arch.components)-1 )
    # Register a function to construct an individual with current app,arch.
    toolbox.register("individual", tools.initRepeat, creator.Individual,
                    toolbox.attr_arch, len(app.kpn_elements))

    # Use this function to create a population of individuals
    toolbox.register("population", tools.initRepeat, list,
                     toolbox.individual)

    # The operators.

    # Pass project, app and arch to the do_evaluate() function because
    # they are needed to run and repair mappings.
    toolbox.register("evaluate", dsehelpers.do_evaluate_mapping, project,
                     app, arch)

    # Mutate using the range: [0, nr_arch_components].
    # App and arch passed to repair invalid mappings.
    toolbox.register("mutate", dsehelpers.do_mutate, app, arch,
                     range(len(arch.components)))
    toolbox.register("mate", dsehelpers.do_crossover, app, arch)
    toolbox.register("select", tools.selTournament, tournsize=3)

    main()

