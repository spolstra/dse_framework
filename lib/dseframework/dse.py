"""Design Space Exploration script for Sesame and ESPAM.

   Prerequisites:
   - DEAP
   - MappingModule library
   - Sesame installation (only for Sesame DSE)

   Usage options:
   - Run the script from the command line. Get usage information with:
        python dse.py --help
   - Run as library. First call the setup function to initialise the DEAP
     toolbox. And then start the DSE with the run function.
"""

import sys
import os
import random
import logging
import argparse
import multiprocessing
import shutil
import pickle
import numpy
import subprocess
import matplotlib.pyplot as plt
import pylab
from functools import partial

import deap
from pyyml import ymlmap
from deap import base
from deap import creator
from deap import tools

from . import dsehelpers
from . import algorithms
from sesamemap.optimize import chromosome
from espam import espammap

REQ_DEAP_VERSION = '1.0'
if (deap.__version__ < REQ_DEAP_VERSION):
    print "You are using DEAP version %s" % deap.__version__
    print "The DSE Framework needs version >=%s" % REQ_DEAP_VERSION
    exit(1)

# Worker threads use tmpdir to copy the project to their working directory.
tmpdir = 'tmpdir'


def generate_individual_single(container, n, m, n_channels=0, dest=0):
    """Generator function that returns an individual that represents a
    mapping of n tasks onto m processors.
    Mapping strategy: map all tasks onto the same processor. Cycle through
    all processors."""
    while True:
        for comp in range(m):
            yield generate_individual(container, partial(int, comp),
                                      n, n_channels, dest)


def generate_individual(container, gen_func, n, n_channels=0, dest=0):
    """Return an mapping individual.
    If n_channels in 0, both tasks and channels are mapped to random
    components. Otherwise all channels are mapped to 'dest'. """
    return container([gen_func() for _ in xrange(n - n_channels)]
                     + [dest for _ in xrange(n_channels)])


def setup(name, maxlength, seed=None, nworkers=1, store_maps=False,
          energy=False, balance=False, pop_init=None, pop_init_single=False,
          no_traces=False, tasks_only=False, no_dfa=False):
    """Configure the dse toolbox.

    Arguments:
    name      -- project filename. YML or ESPAM
    maxlength -- A limit on the length of the communication path at
                 architecture level. Useful for NOC platforms that
                 limited the path length over the network.
    seed      -- Random number generator seed, (default random integer)
    nworkers  -- Number of worker processes for concurrent execution.
                 (default 1)
    """
    logging.info("DSE setup, nworkers: %s" % nworkers)

    # Random seed printed to repeat DSE run with the same seed if needed.
    if seed is None:
        seed = random.randint(0, sys.maxsize)
    logging.info("Using random seed: %d" % seed)
    random.seed(seed)

    # Read project data.
    logging.info("Reading project file...")
    if name.endswith('.yml'):
        (app, arch, project) = dsehelpers.readProject(name, tasks_only,
                                                      maxlength, no_dfa)
    elif name.endswith('.espam'):
        (app, arch, project) = dsehelpers.readEspamProject(name)
    else:
        logging.error("Unknown project format: %s, exiting.." % name)
        exit(1)

    # Initialize project: clean, compile and create application traces
    # Do this before we initialise the Pool of processes, so they all
    # copy the compiled project into their working directory.
    logging.info("Building project...")
    if not no_traces:
        project.clean()

    project.build_all()

    if not no_traces:
        project.trace()

    logging.info("Application model has {} processes and {} channels".format(
        len(app.processes), len(app.channels)))
    logging.info("Channel targets that are memories: {}".format(
        arch.memory_components))

    n_channels, channel_dest_id = dsehelpers.get_channels_and_dest(
            tasks_only, app, arch)

    # Code belows initialises the DEAP. See http://code.google.com/p/deap/
    # Minimize objectives (can be execution time, cost or power)
    logging.info("Setting up DEAP: weight function...")
    creator.create("FitnessMin", base.Fitness, weights=(-1.0, -1.0))
    logging.info("Setting up DEAP: Individuals...")
    creator.create("Individual", list, fitness=creator.FitnessMin)

    # Setup the toolbox with helper functions.
    toolbox = base.Toolbox()

    # An individual is initialised as a random sequence. Sequences that
    # are invalid mappings are repaired before they are evaluated.
    logging.info("Registering DEAP toolbox functions...")

    # Set method to initialize population.
    # Note: This code assumes that processor id's are 0 .. num_procs-1
    # This is true because components is defined in Architecture as:
    # self.processors + self.memories + self.interconnects
    if pop_init is None:
        # pop_init gives the range of processors that we can map onto.
        # The default is all architecture components.
        pop_init = len(arch.components) - 1
    if pop_init_single:
        logging.info("Init pop with single mappings for %d procs" % pop_init)
        n = len(app.mapped_elements)
        toolbox.register("individual",
                         generate_individual_single(creator.Individual,
                                                    n, pop_init,
                                                    n_channels,
                                                    channel_dest_id).next)
    else:
        logging.info("Init population with range [0,%d]" % pop_init)
        toolbox.register("attr_arch", random.randint, 0, pop_init)
        # Register a func to construct an ind. with current app,arch.
        toolbox.register("individual", generate_individual,
                         creator.Individual, toolbox.attr_arch,
                         len(app.mapped_elements), n_channels,
                         channel_dest_id)

    # Use this function to create a population of individuals
    logging.info("Registering DEAP population...")
    toolbox.register("population", tools.initRepeat, list,
                     toolbox.individual)

    # Initialise component cost table
    cost_table = dsehelpers.get_component_cost(arch)

    # Global cache for all workers to store their simulation results so
    # they can be reused between them.
    manager = multiprocessing.Manager()
    glob_cache = manager.dict()

    # Setup evaluation functions. First objective is cycles, the
    # second objective can be cost or energy.
    if energy:
        logging.info("Objectives: cycle count, energy")
        toolbox.register("evaluate", dsehelpers.cached_do_evaluate_mapping,
                         dsehelpers.extract_cycle_energy, cost_table,
                         glob_cache)
        toolbox.register("objective1", dsehelpers.label_cycles)
        toolbox.register("objective2", dsehelpers.label_energy)
    elif balance:
        logging.info("Objectives: cycle count, balance factor")
        toolbox.register("evaluate", dsehelpers.cached_do_evaluate_mapping,
                         dsehelpers.extract_cycle_balance, cost_table,
                         glob_cache)
        toolbox.register("objective1", dsehelpers.label_cycles)
        toolbox.register("objective2", dsehelpers.label_balance)
    else:
        logging.info("Objectives: cycle count, cost")
        toolbox.register("evaluate", dsehelpers.cached_do_evaluate_mapping,
                         dsehelpers.extract_cycle_cost, cost_table,
                         glob_cache)
        toolbox.register("objective1", dsehelpers.label_cycles)
        toolbox.register("objective2", dsehelpers.label_cost)

    # Mutate using the range: [0, nr_arch_components].
    # App and arch passed to repair invalid mappings.
    toolbox.register("mutate", dsehelpers.do_mutate, app, arch,
                     range(len(arch.components)), n_channels)
    # TODO: maybe always exclude channels from crossover. Seems to make
    # sense to me.
    toolbox.register("mate", dsehelpers.do_crossover, app, arch,
                     n_channels)
    toolbox.register("select", tools.selNSGA2)

    # cleanup tempory directories.
    dsehelpers.__cleanup_dirs(tmpdir)
    # Copy current project to a temporary tmpdir so the workers can
    # each make their own copy. If they all start recursively copying
    # from '.' they start copying each others data.
    shutil.copytree('.', tmpdir)

    # Use multiprocessing to evaluate the pool.
    pool = multiprocessing.Pool(processes=nworkers,
                                initializer=dsehelpers.init_worker,
                                initargs=[name, maxlength, tmpdir,
                                          store_maps, tasks_only,
                                          no_dfa])
    toolbox.register("map", pool.map)
    return (app, arch, toolbox, project)


def run(toolbox, app, arch, popn, ngen, lam, visualize_mappings, xml_output,
        mutpb=0.1, cxpb=0.5):
    """Run a DSE of app mappings on arch using the dse toolbox.

    The population is created by randomly setting the architecture
    components. These functions have been registered in the setup
    function. A lot of these random individuals will be invalid mappings
    so the whole population is mutated to trigger the repair function on
    all individuals in the population.
    The DSE is run for ngen generations with the algorithms.eaMuPlusLambda
    provided by DEAP.

    Return value: A list of tuples with the objective fitness values for
    every pareto point and a logbook with stats for every generation.

    Side effects:
    - The mapping of every pareto point is written to a mapping
      file. The filename is 'mapi-obj1-obj2.yml' with i in [1..N],
      obj1 and obj2 the value of the two selected objectives.

    Arguments:
    toolbox -- DEAP toolbox as configured in the setup function.
    app     -- YML or ESPAM application model.
    arch    -- YML or ESPAM architecture model.
    """
    # Create population, parento front.
    pop = toolbox.population(n=popn)
    hof = tools.ParetoFront()

    # Check population, useful for testing --tasks_only
    for i in pop:
        logging.debug("Mapping: %s" % i)

    # Repair any invalid mappings created during the random initialisation.
    # fitness will be assigned later.
    for ind in pop:
        dsehelpers.do_repair(app, arch, ind)

    # Record, avg, std, min and max for every generation.
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", numpy.mean, axis=0)
    stats.register("std", numpy.std, axis=0)
    stats.register("min", numpy.min, axis=0)
    stats.register("max", numpy.max, axis=0)

    logging.info("DSE parameters: generations: %d, pop size: %d, "
                 "mutation: %f, crossover: %f" %
                 (ngen, popn, mutpb, cxpb))
    # Start DSE. We can select different evolutionary algorithms from
    # the DEAP Toolbox, or use a custom one (see dse_custom.py)
    logging.debug("prng state before algorithm: %d" %
                  random.randint(0, sys.maxsize))
    # Custom mu+l is still unchanged. (test)
    pop, logbook = algorithms.CustomMuPlusLambda(pop, toolbox, mu=popn,
                                                 lambda_=lam, cxpb=cxpb,
                                                 mutpb=mutpb, ngen=ngen,
                                                 stats=stats, halloffame=hof,
                                                 verbose=False)

    # DSE finished. Print the Pareto front of non-dominated individuals.
    par = []
    for i, e in enumerate(hof):
        logging.info("Mapping nr: %d" % i)
        logging.info("Chromosome: %s" % e)
        # Print mapping of this pareto solution
        m = chromosome.chromosome2apparchmap(app, arch, e)
        logging.info(m)
        # And a list of all the components used
        comps = m.all_mapped()
        logging.info("Using %d component%s: %s" % (len(comps),
                                                   '' if len(comps) == 1
                                                   else 's',
                                                   comps))
        logging.info("%s: %d, %s: %d" % (toolbox.objective1(),
                                         e.fitness.values[0],
                                         toolbox.objective2(),
                                         e.fitness.values[1]))
        par.append(e.fitness.values)
        # Finally generate a yml map file for this point.
        yml_base = "map%d-%d-%d" % ((i,) + e.fitness.values)

        ymlmap.store_mapping("{}.{}".format(yml_base, "yml"), m.export_yml())
        if xml_output:
            espam_map = espammap.to_espam(m.export_yml())
            espammap.to_file(espam_map, "{}.{}".format(yml_base, "xml"))

        if visualize_mappings:
            try:
                subprocess.call(["MapToDot",
                                 "--png",
                                 "{}.{}".format(yml_base, "yml"),
                                 arch.filename])
            except Exception as e:
                logging.warn("Cannot convert mapping {} to png: {}".format(
                    yml_base, e))
    return par, logbook


def main():
    # Handle arguments
    argparser = argparse.ArgumentParser(description="Design Space Explorer")
    argparser.add_argument("project", help="Sesame project yml file")
    argparser.add_argument("-l", "--log",
                           help="Set logging level (INFO/DEBUG/WARNING)")
    argparser.add_argument("-p", "--maxlength", type=int,
                           default=sys.maxsize,
                           help="Maximum length of feasible path "
                                "(useful for NOC platforms)")
    argparser.add_argument("-j", "--jobs", type=int, default=1,
                           help="Number of concurrent worker processes "
                                "(default: 1)")
    argparser.add_argument("--pop", type=int, default=20,
                           help="Population size (default: 20)")
    argparser.add_argument("--gen", type=int, default=4,
                           help="Number of generations (default: 4)")
    argparser.add_argument("-s", "--seed", type=int,
                           help="Seed to initialise the random number "
                                "generator (default: random)")
    argparser.add_argument("--mutation", type=float, dest='mutpb', default=0.1,
                           help="Probability that offspring is produced by "
                                "mutation (default: 0.1)")
    argparser.add_argument("--crossover", type=float, dest='cxpb', default=0.5,
                           help="Probability that offspring is produced by "
                               "crossover (default: 0.5)")
    argparser.add_argument("--lambda", type=int, dest='lambda_', default=20,
                           help="Lambda: Number of children in "
                                "mu{+,}lambda strategy (default: 20)")
    argparser.add_argument("--nograph", action='store_true', default=False,
                           help="Don't display pareto front graph")
    argparser.add_argument("--store", action='store_true', default=False,
                           help="Store all mappings")
    argparser.add_argument("--energy", action='store_true', default=False,
                           help="Set energy as second objective "
                                "(default is cost)")
    argparser.add_argument("--balance", action='store_true', default=False,
                           help="Set balance as second objective "
                                "(default is cost)")
    argparser.add_argument("--pop_init", type=int, default=None,
                           help="Limit initial population to processor set "
                                "[0,pop_int>")
    argparser.add_argument("--single_init", action='store_true',
                           default=False,
                           help="Initial population with all possible "
                                "all-to-one mappings")
    argparser.add_argument("--zip", action='store_true', default=False,
                           help="Collect pareto mappings in "
                                "'pareto-mappings.tgz'")
    argparser.add_argument("--tasks_only", action='store_true',
                           default=False,
                           help="Only map tasks, "
                           "use default channel mapping")
    argparser.add_argument("--no_dfa", action='store_true',
                           default=False,
                           help="Turn of Data flow analyis")
    argparser.add_argument("-v", "--visualize_mappings", action='store_true',
                           default=False,
                           help="Generate png image files for all pareto "
                                "mappings.")
    argparser.add_argument("--xml", action='store_true', default=False,
                           help="Output mappings in Espam XML format")
    args = argparser.parse_args()

    if args.energy and args.balance:
        print "Error: Energy and balance both selected as second objective"
        exit(1)

    if args.mutpb + args.cxpb > 1.0:
        print "Error: Sum of mutation and crossover probability must be equal or smaller than 1.0"
        exit(1)

    # Set logging level.
    if args.log:
        log_level = getattr(logging, args.log.upper(), None)
        if not isinstance(log_level, int):
            raise ValueError('Invalid log level:%s' % args.log)
        logging.basicConfig(level=log_level)
    else:
        logging.basicConfig(level=logging.INFO)

    logging.info("Using DEAP version: {}".format(deap.__version__))
    # Setup DSE toolbox.
    (app, arch, toolbox, _) = setup(args.project, args.maxlength,
                                    args.seed, nworkers=args.jobs,
                                    store_maps=args.store,
                                    energy=args.energy,
                                    balance=args.balance,
                                    pop_init=args.pop_init,
                                    pop_init_single=args.single_init,
                                    tasks_only=args.tasks_only,
                                    no_dfa=args.no_dfa)

    # Run the DSE experiment.
    par, logbook = run(toolbox, app, arch, popn=args.pop, ngen=args.gen,
                       lam=args.lambda_, mutpb=args.mutpb, cxpb=args.cxpb,
                       visualize_mappings=args.visualize_mappings,
                       xml_output=args.xml)

    # Print the values pairs of the Pareto front.
    logging.info("The exploration generated the following Pareto Front:")
    print par

    # Print & Save logbook
    with open('logbook.data', 'w') as lb_file:
        pickle.dump(logbook, lb_file)
    print logbook

    # Always save pareto data
    with open('pareto.data', 'w') as par_file:
        pickle.dump(par, par_file)
    # ..but only create a plot if we have a display
    if os.environ.get('DISPLAY') is not None and not args.nograph:
        x, y = zip(*par)
        plt.scatter(x, y, s=80, facecolors='red', edgecolors='black')
        plt.xlabel(toolbox.objective1())
        plt.ylabel(toolbox.objective2())
        pylab.savefig('pareto-front.png')
        plt.show()
    if args.zip:
        subprocess.call("tar czf pareto-mappings.tgz map*.yml", shell=True)


# If this script is invoked with "python dseframework/dse.py"
if __name__ == "__main__":
    main()
