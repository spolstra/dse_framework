import logging
import shutil
import argparse
import re
import os.path
import multiprocessing
import functools

import dsehelpers
from pyyml import ymlmap


def do_evaluate_cost(comps, cost_table):
    """Cost of the platform is the sum of the cost of the used
    components."""
    total = 0
    for comp_name in set(comps):
        total += cost_table[comp_name]
    return total


# I cannot reuse the extract_cycle_cost method from dsehelpers because we
# have a ymlmap object m instead of apparchmap object.
def extract_cycle_cost(sim_output, m, cost_table):
    """Extract elapsed time from simulation output."""
    matches = re.findall("Elapsed time: (\d+) units", sim_output)

    try:
        cycles = int(matches[0])
    except (ValueError, IndexError) as e:
        return dsehelpers.simulation_error(sim_output, e, m)

    # Cost is the second objective.
    cost = do_evaluate_cost(ymlmap.to_dict(m, True).values(), cost_table)

    logging.info("cycles %d, cost %d" % (cycles, cost))
    return (cycles, cost)


def extract_cycle_energy(sim_output, m):
    # Extract elapsed time and energy from simulation output.
    cycle_matches = re.findall("Elapsed time: (\d+) units", sim_output)
    power_matches = re.findall("Used energy: (\d+) units", sim_output)

    try:
        cycles = int(cycle_matches[0])
        energy = int(power_matches[0])
    except (ValueError, IndexError) as e:
        return dsehelpers.simulation_error(sim_output, e, m)

    logging.info("cycles %d, energy %d" % (cycles, energy))
    return (cycles, energy)


def evaluate_mapping(map_file, energy=False):
    """simulate project with map_file mapping, extract cycle count
    and cost."""
    shutil.copy(map_file, project.map_yml_file)
    sim_output = project.runarch()
    m = ymlmap.parse(project.map_yml_file)

    if energy:
        objective1, objective2 = extract_cycle_energy(sim_output, m)
    else:
        objective1, objective2 = extract_cycle_cost(sim_output, m,
                cost_table)

    # copy filename to include simulation results.
    logging.info("Mapping: %s" % ymlmap.to_dict(m, True))
    map_base = os.path.splitext(os.path.basename(map_file))[0]
    file_name = "%d-%d-%s" % (objective1, objective2, map_base) + '.yml'
    logging.info("Writing to mapping file: %s" % file_name)
    shutil.copy(map_file, file_name)


def init_worker(name, srcdir):
    """Setup global variables and private working directory.
    These variables are this pool process exclusively.
    """
    global app, arch, project, cache, cost_table
    mydir = multiprocessing.current_process().name
    logging.debug("Current working dir: %s" % os.getcwd())
    logging.debug("Worker %s setting up its working directory" % mydir)
    shutil.copytree(srcdir, mydir)
    os.chdir(mydir)
    logging.debug("Current working dir now: %s" % os.getcwd())
    logging.info("Worker %s: reading project file..." % mydir)
    (app, arch, project) = dsehelpers.readProject(name)
    cost_table = dsehelpers.get_component_cost(arch)


def main():
    logging.basicConfig(level=logging.INFO)
    argparser = argparse.ArgumentParser(description='exhaustive search')
    argparser.add_argument("-j", "--jobs", type=int,
                           default=multiprocessing.cpu_count(),
                           help="Number of concurrent worker processes (default: #cpus)")
    argparser.add_argument('project', help='YML project file')
    argparser.add_argument('mappings', nargs='+',
                           help='Mappings to explore')
    argparser.add_argument("--energy", action='store_true', default=False,
                           help="Set energy as second objective "
                                "(default is cost)")
    args = argparser.parse_args()

    # Initialize project: clean, compile and create application traces
    # Do this before we initialise the Pool of processes, so they all
    # copy the compiled project into their working directory.
    logging.info("Reading project file...")
    (app, arch, project) = dsehelpers.readProject(args.project)
    logging.info("Building project...")
    project.clean()
    project.build_all()
    project.trace()

    # set up common resources
    tmpdir = 'tmpdir'
    dsehelpers.__cleanup_dirs(tmpdir)
    shutil.copytree('.', tmpdir)

    # setup multiprocessing pool
    logging.basicConfig(level=logging.DEBUG)
    pool = multiprocessing.Pool(processes=args.jobs, initializer=init_worker,
                                initargs=[args.project, tmpdir])


    # evaluate all mappings, apply energy argument here.
    pool.map(functools.partial(evaluate_mapping, energy=args.energy),
            args.mappings)


if __name__ == '__main__':
    main()
