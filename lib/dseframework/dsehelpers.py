import os
import logging
import sys
import random
import multiprocessing
import shutil
import re
import glob

from pysesame import sesame
from sesamemap import appyml, archyml
from sesamemap import apparchmap
from sesamemap.optimize import chromosome
from espam import espam, espamapp, espamplatform
from pyyml import yml
import espam.apparchmap as espamapparchmap

# If the architecture does not provide an cost value for a component
# in the yml code, this DEFAULT_COST is used.
DEFAULT_COST = 10
store_mappings = None
app = None
arch = None
project = None


def __cleanup_dirs(tmpdir):
    """Clean up temporary directories used by the worker processes."""
    # Cleanup thread working directories.
    for workerdir in glob.glob('Pool*'):
        shutil.rmtree(workerdir, ignore_errors=True)
    # And tmpdir
    shutil.rmtree(tmpdir, ignore_errors=True)


def init_worker(name, maxlength, srcdir, store_flag, tasks_only, no_dfa):
    """Setup private working directory to run simulations."""
    # Setup a working directory with all the project code here.
    global app, arch, project, store_mappings
    mydir = multiprocessing.current_process().name
    logging.debug("Current working dir: %s" % os.getcwd())
    logging.debug("Worker %s setting up its working directory" % mydir)
    shutil.copytree(srcdir, mydir)
    os.chdir(mydir)
    logging.debug("Current working dir now: %s" % os.getcwd())

    logging.info("Worker %s: reading project file..." % mydir)
    store_mappings = store_flag
    if name.endswith('.yml'):
        (app, arch, project) = readProject(name, tasks_only, maxlength,
                                        no_dfa)
    elif name.endswith('.espam'):
        (app, arch, project) = readEspamProject(name)
    else:
        print "Unknown project format: %s, exiting.." % name
        exit(1)


def get_channels_and_dest(tasks_only, app, arch):
    if tasks_only:
        # Restrict DSE to only explore tasks
        if len(arch.memory_components) != 1:
            raise Exception("Cannot do a task only DSE, there are multiple "
                            "mapping targets for the application channels")

        n_channels = len(app.channels)  # DSE on tasks only
        channel_dest = list(arch.memory_components)[0]  # get only set item
        channel_dest_id = arch.components_index[channel_dest]
        logging.info("Selected tasks only DSE, "
                     "mapping all channels to '{}'".format(channel_dest.name))
        logging.info("Memory has component id {}".format(
            arch.components_index[channel_dest]))
    else:
        n_channels = 0  # 0 indicates: DSE on both tasks and channels.
        channel_dest_id = 0    # No forced destination for the channels.
    return n_channels, channel_dest_id


def readProject(name, tasks_only=False, maxlength=-1, no_dfa=False):
    """Helper function to read a sesame project file."""
    project = sesame.SesameProject(name)
    app_yml, arch_yml, map_yml = project.parse_simulation_yml()
    app = appyml.Application(app_yml)
    if tasks_only or no_dfa:  # No need to do DFA is we only map tasks
        dfa = False
    else:
        dfa = True
    arch = archyml.Architecture(arch_yml, dataflow=dfa, max_length=maxlength)
    logging.debug("the processors: %s" % arch.processors)

    # If there is no mapping file, create a random mapping.
    logging.debug("Checking for mapping file: %s" % map_yml)
    if not os.path.exists(map_yml):
        logging.debug("Mapping file not found, creating random mapping:")
        m = apparchmap.AppArchMap(app, arch)
        if tasks_only:  # might need to fix-up random mapping here
            channels, dest = get_channels_and_dest(tasks_only, app, arch)
            # replace random channel mapping with correct destination
            chromo = list(chromosome.apparchmap2chromosome(m))
            chromo = chromo[:-channels] + [dest] * channels
            logging.debug("Fixed random init mapping: {}".format(chromo))
            m = chromosome.chromosome2apparchmap(app, arch, chromo)
        logging.debug(m)
        project.update_ymlmap(m)
    return (app, arch, project)


def readEspamProject(name):
    """Helper function to read a espam project file."""
    project = espam.EspamProject(name)
    app_xml, arch_xml, map_xml = project.parse_simulation_xml()
    app = espamapp.Application(app_xml)
    arch = espamplatform.EspamPlatform(arch_xml)
    logging.debug("the processors: %s" % arch.processors)

    # We need to create a mapping here if xml map file is missing.
    if not os.path.exists(map_xml):
        m = espamapparchmap.AppArchMap(app, arch)
        project.update_xmlmap(m)
    return (app, arch, project)

# register label functions in the toolbox as "objective1" / "objective2".
def label_cycles():
    return "Cycles"

def label_cost():
    return "Cost"

def label_balance():
    return "Balance factor"

def label_energy():
    return "Energy"

# Memoization wrapper around the evaluation function to speed up the DSE.
# This caching is global to all workers.
def cached_do_evaluate_mapping(extract_func, cost_table, glob_cache, s):
    key = tuple(s)
    if key not in glob_cache:  # check if results is already computed.
        sim_result = do_evaluate_mapping(extract_func, cost_table, s)
        if key in glob_cache:  # result added in the meantime?
            logging.info(
                "Two workers computed same mapping: {}".format(key))
            assert(sim_result == glob_cache[key])
        else:
            glob_cache[key] = sim_result
    return glob_cache[key]


def do_evaluate_mapping(extract_func, cost_table, s):
    """Run simulation to evaluate a mapping instance."""
    # The variables app, arch and project are global variables that
    # have been initialised in init_worker. This way we will not have
    # to read the project for every evaluation. (Passing them as
    # arguments does not work, cannot pickle them)

    # Convert our chromo sequence to a mapping m.
    m = chromosome.chromosome2apparchmap(app, arch, s)
    logging.debug(m)
    logging.debug(m.all_mapped())

    logging.debug("Writing task mappings")
    # Write the mapping to yml file.
    project.update_ymlmap(m)

    # See what our current working directory is
    logging.debug("Current working dir: %s" % os.getcwd())

    # To test our balance objective metric:
    logging.info("Balance metric: %d " % m.max_tasks_on_component())

    # Simulate project and extract the cycle count.
    logging.debug("Running experiment (make run)")

    sim_output = project.runarch(app, arch, m)
    objective1, objective2 = extract_func(sim_output, m, cost_table)

    logging.info("Mapping: %s" % s)
    if store_mappings:
        filename = "%d-%d" % (objective1, objective2) + '-' + \
            '-'.join(str(p) for p in s)
        if len(filename) > 240:
            filename = filename[:240] + '...'
        filename += '_map.yml'
        logging.info("Writing to mapping file: %s" % filename)
        shutil.copy(project.map_yml_file, filename)

    return objective1, objective2


def extract_cycle_cost(sim_output, m, cost_table):
    # Extract elapsed time from simulation output.
    matches = re.findall("Elapsed time: (\d+) units", sim_output)

    try:
        cycles = int(matches[0])
    except (ValueError, IndexError) as e:
        return simulation_error(sim_output, e, m)

    # Cost is the second objective.
    cost = do_evaluate_cost(m.all_mapped(), cost_table)

    logging.info("cycles %d, cost %d" % (cycles, cost))
    return (cycles, cost)


def extract_cycle_energy(sim_output, m, _):
    # Extract elapsed time and energy from simulation output.
    cycle_matches = re.findall("Elapsed time: (\d+) units", sim_output)
    power_matches = re.findall("Used energy: (\d+) units", sim_output)

    try:
        cycles = int(cycle_matches[0])
        energy = int(power_matches[0])
    except (ValueError, IndexError) as e:
        return simulation_error(sim_output, e, m)

    logging.info("cycles %d, energy %d" % (cycles, energy))
    return (cycles, energy)

def extract_cycle_balance(sim_output, m, _):
    # Extract elapsed time from simulation output.
    matches = re.findall("Elapsed time: (\d+) units", sim_output)

    try:
        cycles = int(matches[0])
    except (ValueError, IndexError) as e:
        return simulation_error(sim_output, e, m)

    # Balance is the second objective.
    balance = m.max_tasks_on_component()

    logging.info("cycles %d, balance %d" % (cycles, balance))
    return (cycles, balance)

def simulation_error(sim_output, e, m):
    if 'Processors internal buffers over allocated!' in sim_output:
        # This mapping is invalid, return maxint to make sure
        # it is dominated.
        logging.info(
            "Mapping overallocates buffers: solution will be dominated")
        logging.info("Mapping: %s" % m)
        return (sys.maxsize, sys.maxsize)
    if 'over allocated!' in sim_output: # more general case
        logging.info(
            "Buffer destination overallocated: solution will be dominated")
        logging.info("Mapping: %s" % m)
        return (sys.maxsize, sys.maxsize)
    if 'Error 7' in sim_output:
        # This NOC mapping is invalid, return maxint to make sure
        # it is dominated.
        logging.info("Routing over 3 switches: solution will be dominated")
        logging.info("Mapping: %s" % m)
        return (sys.maxsize, sys.maxsize)
    # An unknown error will stop the DSE.
    print "Unknown error found, output was"
    print sim_output
    raise e


def get_component_cost(arch):
    """Return a table with the costs of all the components in the
    architecture"""
    cost_table = dict()
    for name, comp in arch.component_table.iteritems():
        try:
            cost = int(comp.get_prop_val('cost'))
            logging.info("Assigned cost of %d to component: %s"
                         % (cost, name))
        except yml.YMLError:
            logging.info("Assigned default cost of %d to component: %s"
                         % (DEFAULT_COST, name))
            cost = DEFAULT_COST
        cost_table[name] = cost
    return cost_table


def do_evaluate_cost(comps, cost_table):
    """Cost of the platform is the sum of the cost of the used
    components."""
    total = 0
    for comp in comps:
        total += cost_table[comp.name]
    return total


def do_repair(app, arch, s):
    logging.debug("entered do repair")
    logging.debug(s)

    m = chromosome.chromosome2apparchmap(app, arch, s)
    if m.is_valid():
        logging.debug("mapping is valid")
        return s

    # Not valid so repair individual
    logging.debug("repairing")
    m.repair()
    s[:] = list(chromosome.apparchmap2chromosome(m))
    logging.debug("after repair")
    logging.debug(s)
    logging.debug(m)
    return s


def do_mutate(app, arch, candidates, n_channels, s):
    """Mutate and repair chromosome if needed."""
    logging.debug("Mutate, random sequence now at: %d" %
                  random.randint(0, sys.maxsize))
    chromosome.mutate(s, candidates, n_channels)

    logging.debug(s)
    s = do_repair(app, arch, s)
    return s,


def do_crossover(app, arch, n_channels, s1, s2):
    """Crossover s1 and s2, and repair chromosomes if needed."""
    logging.debug("Crossover, random sequence: %d" %
                  random.randint(0, sys.maxsize))
    chromosome.onepoint_crossover(s1, s2, n_channels)
    logging.debug("crossover")
    logging.debug(s1)
    logging.debug(s2)

    s1 = do_repair(app, arch, s1)
    s2 = do_repair(app, arch, s2)
    return (s1, s2)


def debug_check_for_identicals(pop):
    logging.debug("checking if we have identical individuals in population")
    for ind in pop:
        same_obj_count = 0
        for other in pop:
            if ind is other:
                same_obj_count += 1
        if same_obj_count != 1:
            raise Exception('%s has %d references in population'
                            % (ind, same_obj_count))
    logging.debug("checking if we have identicals: OK")


# Unit tests for helper functions
if __name__ == "__main__":

    random.seed(2)

    # read app and arch from project file
    project = sesame.SesameProject(sys.argv[1])
    app_yml, arch_yml, map_yml = project.parse_simulation_yml()
    app = appyml.Application(app_yml)
    arch = archyml.Architecture(arch_yml)

    print "Operations"
    print app.operations

    # mutate candidates:
    print "Components"
    print arch.components
    print "Candidate ids"
    candidates = range(len(arch.components))
    print candidates

    print "Chromosome"
    c = chromosome.SesameMapChromosome(app, arch)
    print c
    print type(c)
    s = list(c)
    print "Mutating"
    for i in range(19):
        do_mutate(app, arch, candidates, s)
        print s

    m = chromosome.chromosome2apparchmap(app, arch, s)
    print m
    project.update_ymlmap(m)
