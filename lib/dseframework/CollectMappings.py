import argparse
import re
import sys
import pickle
import subprocess

def main():
    argparser = argparse.ArgumentParser(description="CollectMappings")
    argparser.add_argument('--names', '-n', help='YML mapping file names')
    argparser.add_argument('--output', '-o', default='output.data',
            help='Dataset output file')
    argparser.add_argument('--collect', '-c', action='store_true',
            help='Collect file names from PoolWorker directories')
    argparser.add_argument('--pattern', '-p',
            default='PoolWorker-\d/(\d+)-(\d+).*',
            help='File pattern to match. Default "PoolWorker-\d/(\d+)-(\d+).*"')
    args = argparser.parse_args()

    # ls PoolWorker-?/[0-9]*map.yml > all.txt
    # input format is: "PoolWorker-1/526302-2100-1-2-3-1-0-1-4-2-4-4-1_map.yml"
    file_pattern = re.compile(args.pattern)

    # Set defaults
    if args.names:
        f = open(args.names)
    else:
        f = sys.stdin

    cycle_cost = []
    if args.collect:
        ls_command = "ls PoolWorker-?/[0-9]*map.yml"
        for line in subprocess.check_output(ls_command, shell=True, encoding='utf8').split('\n'):
            match = file_pattern.match(line)
            if match:
                cycle_cost.append((int(match.group(1)), int(match.group(2))))
    else:
        for line in f:
            match = file_pattern.match(line)
            if match:
                cycle_cost.append((int(match.group(1)), int(match.group(2))))

    cycle_cost_uniq = list(set(cycle_cost))

    with open(args.output, 'wb') as outfile:
        pickle.dump(cycle_cost_uniq, outfile)

