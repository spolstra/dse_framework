import glob
import re
from collections import defaultdict


def read_results():
    """Read all PoolWorker-? directories."""
    results = defaultdict(list)
    dir_templ = "PoolWorker-*"
    file_templ = "/[0-9]*_map.yml"

    for d in glob.glob(dir_templ):
        mappings = glob.glob(d + file_templ)
        for m in mappings:
            results[d].append(re.sub('PoolWorker-\d\/', '', m))
    return results


def find_others(m, results, exclude):
    """Find m in dict results, exclude if key == exclude"""
    found = []
    for w in results.keys():
        if w == exclude:
            continue
        if results[w].count(m) > 0:
            found.append(w)
            continue
    return found


def print_results(results):
    """nr of evaluated mappings per worker"""
    for (k, v) in results.items():
        print(k, v)


def duplicates(results):
    """check duplicates between workers in results dict"""
    for w in results.keys():
        print("\nChecking mappings for worker {}:".format(w))
        print("Contains {} mappings".format(len(results[w])))
        # for every m: check if m is in one of the other worker lists.
        for m in results[w]:
            others = find_others(m, results, exclude=w)
            if len(others) > 0:
                print(m)
                print(others)
                print()


# read results from worker pool directories
results = read_results()

# print results
print_results(results)

# check duplicates between workes
print("\nChecking for duplicates:")
duplicates(results)

# check for duplicates within a single worker
for w, r in results.items():
    print(len(r), len(set(r)))
