#!/bin/bash

if [ "$#" -ne 3 ]; then
    echo "Usage: $0 <project_name> <nr_dups> <nr_procs>"
    exit 1
fi


# Create the application with duplication. And create a bus architecture
# with 'nr_procs' processors. We need also create a mapping so we can
# compile the project.
make clean
make clean-dse
time DuplicateApplication -n $2 app/$1_app.yml.non_duplicated -o app/$1_app.yml
time GenBusArch -n $3 -o arch/$1_arch.yml
time GenerateMappings app/$1_app.yml arch/$1_arch.yml --nopng -r -n 1
mv map_0.yml $1_map.yml

# Compile and create the application trace files
make
make -C app runtrace

# This is the 'initial state' of a pool worker.
# Now time generating a new mapping and an evaluation run.
time ( GenerateMappings app/$1_app.yml arch/$1_arch.yml --nopng -r -n 1 && \
    mv map_0.yml $1_map.yml && \
    make runarch )
