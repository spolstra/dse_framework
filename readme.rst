Sesame Design space exploration
-------------------------------

The ``dse.py`` and ``dse_custom.py`` scripts perform design space
exploration on Sesame projects.

The scripts use the python Mapping module library from Sesame to
manipulate YML and run simulations. For the evolutionary algorithms
it uses DEAP which is a evolutionary computation framework.

In its current configuration it will perform multi-objective design space
exploration (execution time and cost).  The parameters of the evolutionary
algorithm can be easily changed.

Requirements
------------

* Sesame modeling and simulation framework (from ``https://bitbucket.org/spolstra/sesame_installer/src/master/``)
* MappingModule (installed automatically as part of Sesame)
* Both Sesame and the DSE framework require Python 2.7
* DEAP (as ``python-deap`` via apt-get or from ``http://code.google.com/p/deap/``)
* The matplotlib package (as ``python-matplotlib`` via apt-get)

Installation
------------
Clone this dse_framework with::

    git clone git@bitbucket.org:spolstra/dse_framework.git

Add the ``lib`` directory to your ``PYTHONPATH`` with::

    export PYTHONPATH+=:~/projects/dse_framework/lib

Then add the ``bin`` directory to your path with::

    export PATH+=:~/projects/dse_framework/bin

You should now be able to run the ``RunDSE`` command. Try ``RunDSE -h`` to
get a description of all options.

Tutorial
--------
To get started with the DSE framework check out the tutorial here
https://bitbucket.org/spolstra/dse_framework/src/master/tutorial/tutorial.rst

The tutorial is also available as a pdf file here: https://bitbucket.org/spolstra/dse_framework/raw/HEAD/tutorial/tutorial.pdf

Basic Usage
-----------
The DSE script must be run from a Sesame project directory and needs a
project YML file.  The DSE will work on any Sesame project provided that the
data paths are annotated with the 'datadir' property. The mjpeg and simple
platforms are included as a demo of the DSE.

To perform a DSE on the simple platform you need to type::

    [starting from dse_framework root directory]
    cd simple
    RunDSE simple.yml

The parameters of the evolutionary algorithm are currently still hard coded
in the script. Parameters that can be changed in the script are::

    Population size         POPN (= MU)
    Crossover probability   CXPB
    Mutation probability    MUTPB
    Number of generations   NGEN
    Number of children      LAMBDA
    Selection algorithm     eg. tools.selNSGA2, tools.selTournament
    Evolutionary algorithm  eg. eaSimple, eaMuPlusLambda or a custom alg.


After the exploration is finished the Pareto front will be printed as text
and plotted as a simple graph.
For more information please check out the tutorial document linked above.

Implementation details
----------------------
The ``dse.py`` contains the main evolutionary algorithm loop.
``dse.py`` uses an standard algorithm for the DEAP toolbox, while
``dse_custom.py`` uses a custom algorithm. This allows you to add
additional custom steps when needed.

The file dsehelper.py contains the custom part of the evolutionary
toolbox:

``readProject()``
    A wrapper function to read all the project data.
``do_evaluate_mapping()``
    Evaluate a specific mapping: cost and execution time
``do_mutate``
    Custom mutation function.
``do_crossover``
    Custom crossover function.

A mapping is checked for validity it is changed by mutation of crossover
before it is run.  When two tasks that communicate with each other are
mapped onto different components the mapping of the channel is checked to
see if a data path actually exists with this mapping. If not it will be
repaired. For this to work the communication components will need the
"datadir" properties to indicate the direction of the communication.

Modifications for Exploration
-----------------------------
All the projects in the example directory of the dse_framework are already modified
to be explored.  But a standard Sesame project needs a couple of
modifications to run within the DSE framework in the following files:

Changes to: ``Makefile``
    1. Split ``runtrace`` target into ``trace`` and ``runarch`` targets.
    2. Clean tempdir, map and pool worker directories.
Changes to: ``arch/Makefile``
    1. Collect all components in the architecture YML in the main ps
       file with ``PearlYMLTool --create-all-stub $(ARCH).yml``.
       It is important to use the architecture YML file ``$(ARCH)`` and
       not the combined virtual layer and architecture file
       ``$(VIRTARCH)``, because the latter does not contain all
       templates.
    2. Remove the $(NAME).ps in the ``clean`` target.
Changes to: ``arch/project_arch.yml``
    The ``datadir`` property needs to be specified at the correct
    level in the network, otherwise the data flow analysis will not
    detect it.

The easiest way to modify a Sesame project so that it works with the
DSE Framework is to take replace the two ``Makefiles`` mentioned above
with the ones found in the ``simple`` example in DSE project. The only
thing that needs to be changed after copying these is the ``NAME``
variable::

    NAME = simple

If communication components in the architecture YML are missing the
``datadir`` property the DSE might try to simulate mappings that
are not valid.  In that case the YML of these components
need to be replaced with those found in the DSE examples.

Mapping representation
----------------------
The genetic algorithm maintains a population of application to
architecture mappings. Each mapping in this population is represented by
a chromosome which is just a list of numbers. The index in the list
is the identifier of a task or channel, and the value at that index is
the identifier of an architecture component.

