------------------------------------
Design Space Exploration with Sesame
------------------------------------

Introduction
------------
This tutorial assumes that you have succesfully installed the design space
exploration framework
on your system. Please follow the instructions installation instructions if
you haven't done so yet here:
https://bitbucket.org/spolstra/dse_framework/src/master/readme.rst 

Sesame provides a design space exploration framework that takes an
application model and an architecture platform and explores the
mapping of application tasks and channels on the architecture
components. The framework use the Mapping Module library from
the Sesame project to manipulate YML and run simulations. For the
evolutionary algorithms it uses DEAP which is a evolutionary computation
framework.

This results in a simple but powerful framework that can be used to
quickly create DSE experiments.  In its current configuration it will
perform multi-objective design space exploration with execution time
and cost as the objectives.  The DEAP package makes it very easy to
change parameters and exchange evolutionary algorithms.

The framework includes a number of examples of different
combinations of applications and architectures.  Here we
will show one such example and discuss some of the possibilities of
the DSE framework.

We should note here that the framework is meant to be a starting point
for an experiment.  Depending on the special requirements of the
platform, the DSE framework or project may need to
be modified.

Sobel-Crossbar Example
----------------------
We will now walk through a simple design space exploration experiment
where we map the Sobel application onto a crossbar architecture
connected to 4 MicroBlaze processors.

The DSE example will evaluate a population of 10 mappings and will run for
20 generations.  Every architecture component can have a cost value that is
specified in the architecture description with a cost property. If no cost
is specified a default cost of 10 is assumed. The total cost of a mapping is
the sum of all mapped components.  Every mapping is simulated with Sesame
and the resulting cost/performance pair is printed after every simulation.

The exploration example can be found in the directory
``dse_framework/sobel-crossbar``. The command to perform the mapping
exploration is called ``RunDSE`` and it must be executed from the root of project
directory that we want to explore. The only mandatory argument is the
project yml file. To start the experiment goto the ``sobel-crossbar``
directory and type::

    $ RunDSE -s 1 --pop 10 --gen 20 -j 4 -v sobel.yml 

In addition to the mandatory project file ``sobel.yml`` we also specify the
following optional arguments::

-s 1        Fix the random seed so we can reproduce the experiment.
--pop 10    Set the population size to 10 individuals.
--gen 20    Run the DSE for 20 generations.
-j 4        Use 4 worker threads to evaluate individuals in parallel.
-v          Visualize the pareto optimal mappings that are found.

``RunDSE`` has a lot more options, for a short description of all the options
invoke ``RunDSE`` with the ``-h`` flag.

First the population is initialised with 10 valid random mappings.
Then the main loop of the genetic algorithm will start evaluating the
mappings in the population.  After 20 generations the exploration ends
and all the pareto optimal mappings will be shown in a graph window and in text
form in the terminal.

Lets look at the text in the terminal first. When the exploration finishes
first all the pareto optimal mappings are printed, then a list of the
objective values and finally a table containing some statistics of every
generation. 
So if we scroll up past the statistics and objective values we see the last
mapping represented in text form::

    [..]
    INFO:root:Mapping nr: 12
    INFO:root:Chromosome: [2, 2, 2, 2, 2, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6]
    INFO:root:Kahn process/channel  ==> Architecture component Template
    Kahn process ND_0     ==> Processor P2           vproc
    Kahn process ND_1     ==> Processor P2           vproc
    Kahn process ND_2     ==> Processor P2           vproc
    Kahn process ND_3     ==> Processor P2           vproc
    Kahn process ND_4     ==> Processor P2           vproc
    ND_0.OG_1->ND_1.IG_1  ==> Memory VB2             vbuffer
    [..]

Every pareto optimal mapping is printed in two ways, first as a sequence of
architecture identifiers and then as a more readable mapping table.  The
chromosome sequence gives the architecture component identifiers for every
task and channel in the application. The tasks entries are followed by the
channel entries. In the mapping above all are mapped onto architecture
component 2 (Processor P2). All channels are mapped onto architecture
component 6 (VB2). After the chromosome sequence this mapping information is
again shown as a mapping table.  Only one channel mapping is shown here.

After the mapping all mapped architecture components are listed.
This mapping only uses two components, one processor and one
virtual buffer. The last entry of this mapping is a tuple of the cycle
count and the cost objectives of this mapping::

    INFO:root:Using 2 components: [P2, VB2]
    INFO:root:Cycles: 107290506, Cost: 20
    [..]

Finally the framework shows the cost and performance values of the 13 pareto
optimal mappings that were found, there are multiple mappings that have the
same cost and performance::

    INFO:root:The exploration generated the following Pareto Front:
    [(46901524.0, 60.0), (46901524.0, 60.0), (46901524.0, 60.0),
    (46901524.0, 60.0), (46901524.0, 60.0), (4690 1564.0, 50.0),
    (46901564.0, 50.0), (57706137.0, 40.0), (57706137.0, 40.0), (88360568.0,
    30.0), (88360568.0 , 30.0), (88360568.0, 30.0), (107290506.0, 20.0)]

These values form the pareto front. The pareto front data is stored in the
file ``pareto.data`` and can be visualized with the command ``PrintPareto
pareto.data``. At the end of the exploration ``RunDSE`` will run this
command by default so a window will open with a graph of the pareto front
as shown in the figure below.

.. figure:: pareto-front-sobel-crossbar.png
   :scale: 60
   :alt: Output: Pareto front of Sobel application mapped onto a crossbar architecture.

   Fig1: Pareto front of our example exploration of mapping the Sobel application onto a crossbar architecture.

All pareto optimal mappings descriptions are also be written to file. The filename
format is ``mapN.yml``. Since we specified the ``-v`` flag every mapping YML
description is also visualized as a PNG image file called ``mapN.png``.
The figure below shows one of the mappings that was found during our
exploration.

.. figure:: map6-46901564-50.png
   :scale: 135
   :alt: Output: One of the mappings found during our exploration.

   Fig2: A pareto optimal mappings found during the exploration.

If you forgot to pass the ``-v`` flag to visualize the mappings you can
still generate the PNG image files by manually running this command from the
root directory of the project where the ``mapN.yml`` files reside::

    $ for i in `ls map*yml`; do
        MapToDot $i arch/sobel_arch.yml;
        dot -T png dgraph.dot > $(basename $i .yml).png;
      done

During the exploration some statistics are stored about every generation.
This data is shown in text form in the terminal and also stored in the data file
``logbook.data``. To plot the best fitness value of each objective in every
generation during the exploration run the command ``PrintLogBook
logbook.data``. The figure below shows that the exploration could not find a
mapping that improved the performance of the best mapping in the randomly
first generation. It did improve on the cost and found cheaper architectures
during the exploration.  The fastest mapping uses three of the four
processors on the platform and the cheapest mapping uses only a one
processor.

.. figure:: perf-cost.png
   :scale: 60
   :alt: Output: text

   Fig3: The best values of the two objectives cost and cycle count in every generation.


Customisation
-------------

The exploration framework is meant as a starting point. The parameters
of this tutorial are chosen in an adhoc manner. For a real exploration
you need to experiment with different values. The following
table list some of the parameters that can be changed. The current
default settings are shown in boldface.

+-----------------------+-----------------------------------------------+
| Description           | Name                                          |
+=======================+===============================================+
|Population size        |POPN **(20)**                                  |
+-----------------------+-----------------------------------------------+
|Crossover probability  |CXPB **(0.5)**                                 |
+-----------------------+-----------------------------------------------+
|Mutation probability   |MUTPB **(0.1)**                                |
+-----------------------+-----------------------------------------------+
|Number of generations  |NGEN **(4)**                                   |
+-----------------------+-----------------------------------------------+
|Number of children     |LAMBDA **(20)**                                |
+-----------------------+-----------------------------------------------+
|Evolutionary toolbox   | mutate, crossover and evaluation functions    |
+-----------------------+-----------------------------------------------+
|Selection algorithm    |eg. **tools.selNSGA2** or tools.selTournament  |
+-----------------------+-----------------------------------------------+
|Evolutionary algorithm |eg. eaSimple, **eaMuPlusLambda** or custom     |
+-----------------------+-----------------------------------------------+

Some of the parameters such as the number of generations, the number of
children and the population size can be specified on the command line,
others need to be changed in the code.
In addition to changing the parameters, special requirements will
require changing the exploration script itself.  An example of such a
change could be the cost calculation of the crossbar platform. The
cost is currently based on the number of virtual buffers that are
used. You could argue that using the crossbar is an all of nothing
affair, either you do or you don't.  This means you would need to
change the cost calculation function.

..
    Notes (maybe not included in the tutorial document)
    [obtaining the framework]
    Should be downloadable as part of Sesame. Don't discuss it here.

    [too detailed]
    feasibility checking, modifying architecture: add datadir when needed.

    When combining an application and an architecture the main project yml
    file and the main Makefile need to correctly name the application and
    architecture.

    targets runarch, trace

    [1] https://code.google.com/p/deap/
    [.] location of dse_frame (git or tar)
