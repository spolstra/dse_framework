doc: readme.rst
	rst2pdf $<
	make -C tutorial

tags:
	ctags `find . -name \*py`

clean:
	make -C examples clean

dist:
	git archive --format=tar --prefix=dse_framework/ HEAD > dse_framework.tar
