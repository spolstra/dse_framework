/***************************************************************************************
 * 	File: mbLayer.h	
 * 	Description: header file for Macroblock Layer FSM
 * 	Author: Adarsha Rao
 * 	Copyright Information: Morphing Machines Pvt. (Ltd. IISc New Technology Incubation).
 * 						   All rights reserved.
 * 	Last Modified:    09-19-2008
 * 	Revision History: 
 * 		09-19-2008 	Adarsha 	Initial Version
 * ---------------------------------------------------------------------------------
 ***************************************************************************************/

#ifndef MB_LAYER_H_
#define MB_LAYER_H_

#define MAX_PIC_WIDTH 32

typedef enum MB_TYPE_DEF  { 
				 B_DIRECT_16x16 = 0,
				 B_L0_16x16,
				 B_L1_16x16,
				 B_Bi_16x16,
				 B_L0_L0_16x8,
				 B_L0_L0_8x16,
				 B_L1_L1_16x8,
				 B_L1_L1_8x16,
				 B_L0_L1_16x8,
				 B_L0_L1_8x16,
				 B_L1_L0_16x8,
				 B_L1_L0_8x16,
				 B_L0_Bi_16x8,
				 B_L0_Bi_8x16,
				 B_L1_Bi_16x8,
				 B_L1_Bi_8x16,
				 B_Bi_L0_16x8,
				 B_Bi_L0_8x16,
				 B_Bi_L1_16x8,
				 B_Bi_L1_8x16,
				 B_Bi_Bi_16x8,
				 B_Bi_Bi_8x16,
				 B_8x8,
				 P_L0_16x16,
				 P_L0_L0_16x8,
				 P_L0_L0_8x16,
				 P_8x8,
				 P_8x8ref0,
				 I_NxN,
				 I_16x16_0_0_0,
				 I_16x16_1_0_0,
				 I_16x16_2_0_0,
				 I_16x16_3_0_0,
				 I_16x16_0_1_0,
				 I_16x16_1_1_0,
				 I_16x16_2_1_0,
				 I_16x16_3_1_0,
				 I_16x16_0_2_0,
				 I_16x16_1_2_0,
				 I_16x16_2_2_0,
				 I_16x16_3_2_0,
				 I_16x16_0_0_1,
				 I_16x16_1_0_1,
				 I_16x16_2_0_1,
				 I_16x16_3_0_1,
				 I_16x16_0_1_1,
				 I_16x16_1_1_1,
				 I_16x16_2_1_1,
				 I_16x16_3_1_1,
				 I_16x16_0_2_1,
				 I_16x16_1_2_1,
				 I_16x16_2_2_1,
				 I_16x16_3_2_1,
				 I_PCM
			}MB_TYPE;

typedef enum SUBMB_TYPE_DEF  {
					B_DIRECT_8x8 = 0,
					B_L0_8x8,
					B_L1_8x8,
					B_Bi_8x8,
					B_L0_8x4,
					B_L0_4x8,
					B_L1_8x4,
					B_L1_4x8,
					B_Bi_8x4,
					B_Bi_4x8,
					B_L0_4x4,
					B_L1_4x4,
					B_Bi_4x4,
					P_L0_8x8,
					P_L0_8x4,
					P_L1_4x8,
					P_L0_4x4
			}SUBMB_TYPE;

typedef enum MB_PRED_MODE_DEF  {
						Intra_16x16,
						Intra_4x4,
						Intra_8x8,
						Pred_L0,
						Pred_L1,
						BiPred,
						Direct
			}MB_PRED_MODE;


typedef struct IntraPredModeContextInfo
{
		int mb_x;
		unsigned char leftAvailable;
		unsigned char  topAvailable;
		unsigned char leftInter;
		unsigned char topInter;
		unsigned char leftIntra4x4; 
		unsigned char topIntra4x4;
}IntraPredModeContextInfo;

typedef struct MblStruct{
	unsigned long mbNum;
	unsigned long mb_y;
	unsigned long mb_x;
	MB_TYPE mbType;
	
	unsigned char skipped;
	MB_PRED_MODE mbPredMode;
	unsigned short cbpLuma;
	unsigned short cbpChroma;
	char qPy; 
	char qPc;

	///////////////////// context informtion //////////
	unsigned long mbAddrA;
	unsigned char leftAvail;
	unsigned char leftSkipped;
	unsigned char leftInter;
	unsigned char leftIntra4x4;
	unsigned char cbpLumaLeft;
	unsigned char cbpChromaLeft;
	int qPyLeft;
	int qPcLeft;

	unsigned long mbAddrB;
	unsigned char topAvail;
	unsigned char topSkipped;
	unsigned char topInter;
	unsigned char topIntra4x4;
	unsigned char cbpLumaTop;
	unsigned char cbpChromaTop;
	int qPyTop;
	int qPcTop;

	////////////// all these params used for intra prediction //////////////
	unsigned char Intra4x4PredMode[16];
	unsigned char intraChromaPredMode;
	unsigned char intra16x16PredMode;
}MblStruct;

void parseMacroBlockLayer(nalUnit *nal, SpsStruct *sps, PpsStruct *pps,
		ShStruct *sliceHeader, MblStruct *Mbl, dword *bitPosIn, dword *bitPosOut);
void getMbType(nalUnit *nal, ShStruct *sliceHeader, MblStruct *Mbl, dword *bitPosIn, dword *bitPosOut);
void mbPred (nalUnit *nal, SpsStruct *sps, PpsStruct *pps,
		ShStruct *sliceHeader, MblStruct *Mbl, dword *bitPosIn, dword *bitPosOut);
void getMappedEg (nalUnit *nal, SpsStruct *sps, MblStruct *Mbl, dword *bitPosIn, dword *bitPosOut);
void calcIntra4x4PredMode(IntraPredModeContextInfo params, unsigned char prevIntra4x4PredMode[16], 
		unsigned char remIntra4x4PredMode[16], unsigned char Intra4x4PredMode[16]);
unsigned char getI16x16PredMode(MB_TYPE mbType);
#endif
