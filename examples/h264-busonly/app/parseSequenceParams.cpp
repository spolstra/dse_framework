/***************************************************************************************
 * 	File: 	sps.c
 * 	Description: Implements sequence parameter set FSM 
 * 	Author: Adarsha Rao
 * 	Copyright Information: Morphing Machines Pvt. (Ltd. IISc New Technology Incubation).
 * 						   All rights reserved.
 * 	Last Modified:    09-19-2008
 * 	Revision History: 
 * 		09-19-2008 	Adarsha 	Initial Version
 * ---------------------------------------------------------------------------------
 ***************************************************************************************/

#include "decoderCommon.h"

//extern FILE *tracefile;

void seqParamSet(nalUnit *nal, SpsStruct *Sps, dword *bitPosIn, dword *bitPosOut)
{

	unsigned short videoData;

	Sps->profileIdc = 0;
	Sps->constraint_set0_flag 				= 0;
	Sps->constraint_set1_flag 				= 0;
	Sps->constraint_set2_flag 				= 0;
	Sps->constraint_set3_flag 				= 0;
	Sps->levelIdc 							= 0;
	Sps->seqParamSetId 					= 0;
	Sps->chromaFormatIdc 					= 1;
	Sps->residualColorTransformFlag 		= 0;
	Sps->bitDepthLumaMinus8 				= 0;
	Sps->bitDepthChromaMinus8 				= 0;
	Sps->qpPrimeYZeroTransformBypassFlag 	= 0;
	Sps->seqScalingMatrixPresentFlag 		= 0;
	Sps->log2MaxFrameNumMinus4 			= 0;  			
	Sps->picOrderCntType 					= 0; 
	Sps->log2MaxPicOrderCntLsbMinus4 		= 0;
	Sps->deltaPicOrderAlwaysZeroFlag 		= 0;
	Sps->numRefFrames 						= 0;
	Sps->gapsInFrameNumAllowedFlag 		= 0;
	Sps->picHeightInMbs 					= 0;
	Sps->picWidthInMbs 					= 0;
	Sps->frameMbsOnlyFlag 					= 0;
	Sps->frameCropBottomOffset 			= 0;
	Sps->vuiParametersPresentFlag 			= 0;
	Sps->frameCropTopOffset 				= 0;
	Sps->frameCropRightOffset 				= 0;
	Sps->frameCropLeftOffset 				= 0;
	Sps->frameCroppingFlag 				= 0;
	Sps->direct8x8InferenceFlag 			= 0;
	Sps->mbAdaptiveFieldFrameFlag 			= 0;

	videoData = getVideoData(nal,FLC,16);
	Sps->profileIdc = (videoData >> 8) & 0xff;
	Sps->constraint_set0_flag = (videoData >> 7) & 0x01;
	Sps->constraint_set1_flag = (videoData >> 6) & 0x01;
	Sps->constraint_set2_flag = (videoData >> 5) & 0x01;
	Sps->constraint_set3_flag = (videoData >> 4) & 0x01;

	videoData = getVideoData(nal,FLC,8);
	Sps->levelIdc = (videoData >> 8) & 0xff;

	videoData = getVideoData(nal,UE,0);
	Sps->seqParamSetId = videoData;

	videoData = getVideoData(nal,UE,0);
	Sps->log2MaxFrameNumMinus4 = videoData;

	videoData = getVideoData(nal,UE,0);
	Sps->picOrderCntType = videoData;
	
	if(Sps->picOrderCntType == 0)
	{
		videoData = getVideoData(nal,UE,0);
		Sps->log2MaxPicOrderCntLsbMinus4 = videoData;
	}

	videoData = getVideoData(nal,UE,0);
	Sps->numRefFrames = videoData;

	videoData = getVideoData(nal,FLC,1);
	Sps->gapsInFrameNumAllowedFlag = (videoData >> 15) & 0x01;

	videoData = getVideoData(nal,UE,0);
	Sps->picWidthInMbs = videoData + 1;

	videoData = getVideoData(nal,UE,0);
	Sps->picHeightInMbs = videoData + 1;

	videoData = getVideoData(nal,FLC,1);
	Sps->frameMbsOnlyFlag = (videoData >> 15) & 0x01;

	if(!Sps->frameMbsOnlyFlag)
	{
		videoData = getVideoData(nal,FLC,1);
		Sps->mbAdaptiveFieldFrameFlag = (videoData >> 15) & 0x01;
	}

	videoData = getVideoData(nal,FLC,1);
	Sps->direct8x8InferenceFlag = (videoData >> 15) & 0x01;

	videoData = getVideoData(nal,FLC,1);
	Sps->frameCroppingFlag = (videoData >> 15) & 0x01;

	videoData = getVideoData(nal,FLC,1);
	Sps->vuiParametersPresentFlag = (videoData >> 15) & 0x01;

//fprintf(tracefile,"profileIdc = %d\n",Sps->profileIdc );
//fprintf(tracefile,"levelIdc = %d\n",Sps->levelIdc);
//fprintf(tracefile,"seqParamSetId = %d\n",Sps->seqParamSetId );
//fprintf(tracefile,"chromaFormatIdc = %d\n",Sps->chromaFormatIdc );
//fprintf(tracefile,"log2MaxFrameNumMinus4 = %d\n",Sps->log2MaxFrameNumMinus4);
//fprintf(tracefile,"picOrderCntType = %d\n",Sps->picOrderCntType);
//fprintf(tracefile,"log2MaxPicOrderCntLsbMinus4  = %d\n",Sps->log2MaxPicOrderCntLsbMinus4 );
//fprintf(tracefile,"picWidthInMbs = %d\n",Sps->picWidthInMbs );
//fprintf(tracefile,"picHeightInMbs = %d\n",Sps->picHeightInMbs );
//fprintf(tracefile,"frameMbsOnlyFlag = %d\n",Sps->frameMbsOnlyFlag );
}

