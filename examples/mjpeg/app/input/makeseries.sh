#! /bin/sh

if test $# -lt 2; then
    echo "$0 [target base] [source files] . . ."
    exit 1
fi

base=$1
count=0
shift 1

while test ${count} -le $#; do
    base_count=`printf "%s%4.4d" ${base} ${count}`
    echo $1 "-->" ${base_count}

    convert $1 -geometry 352!x288! ${base_count}_tmp.jpg
    convert ${base_count}_tmp.jpg -interlace partition RGB:${base_count}
    rm ${base_count}_tmp.jpg
    count=`expr ${count} + 1`
    shift 1
done
