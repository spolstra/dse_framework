/*******************************************************************\

       SESAME project software license

        Copyright (C) 2005 University of Amsterdam

    This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
         GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
           02111-1307, USA.

      For information regarding the SESAME software project see
        http://sesamesim.sourceforge.net or email
        jcofflan@users.sourceforge.net

\*******************************************************************/
#ifndef VIDEO_OUT_H
#define VIDEO_OUT_H

#include "Video_out_Base.h"

#include "stdio.h"
#include <string>


class Video_out : public Video_out_Base
{
        // the number of the color components in the frame = [1,255]
        static const int GlobalNumberComponents = 3;
        // the number of the color components in the scan = [1,4]
        static const int NumberComponents = 3;

        int hf[GlobalNumberComponents], vf[GlobalNumberComponents], tq[GlobalNumberComponents];
        int ci[NumberComponents], td[NumberComponents], ta[NumberComponents];
        int SSS, SSE, SAH, SAL;
        int V_size, H_size;
        FILE *fh;
        std::string filename;

    public:
        Video_out(Id n, Video_out_Ports *ports);
        virtual ~Video_out();

        FILE *mwopen(const char *filename);
        void mwclose(FILE *fh);
        void bputc(int c);
        void WriteSoi();
        void WriteJfif();
        void WriteEoi();
        void WriteSof();
        void WriteSos();
        void WriteDqt(int *QLumMatrix, int *QChromMatrix);
        void WriteDht(TTablesInfo *Lum, TTablesInfo *Chrom);
        void WriteHuffman(int *bits, int *huffval);

        void main();
};
#endif // VIDEO_OUT_H
