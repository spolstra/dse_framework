import unittest
import sys
import os
import logging
import shutil
import multiprocessing as mp

from dseframework import dse, dsehelpers

class TestMJPEG(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # Need to change to script directory to run testcase
        cwd = os.path.dirname(os.path.abspath(__file__)) + '/'
        os.chdir(cwd)
        logging.basicConfig(level=logging.WARNING)

    def setUp(self):
        # Experiment parameters.
        shutil.copyfile('mjpeg_map.yml-ref', 'mjpeg_map.yml')
        self.name = "mjpeg.yml"
        self.maxlength = sys.maxint
        self.npop = 20
        self.lambda_ = 20
        self.ngen = 4

    def tearDown(self):
        self.project.clean()

    def test_mjpeg1(self):
        # Random seed and matching reference pareto front.
        seed = 42
        ref_pareto = [(36643524.0, 50.0), (38218179.0, 40.0)]
        self.run_and_compare(seed, ref_pareto)

    def test_mjpeg2(self):
        # Random seed and matching reference pareto front.
        seed = 1
        ref_pareto = [(36643893.0, 50.0), (39972082.0, 40.0)]
        self.run_and_compare(seed, ref_pareto)

    def test_mjpeg3(self):
        # Random seed and matching reference pareto front.
        seed = 100
        ref_pareto = [(36643748.0, 60.0), (37341314.0, 50.0),
                      (38242921.0, 40.0)]
        self.run_and_compare(seed, ref_pareto)

    def run_and_compare(self, seed, ref_pareto):
        (app, arch, toolbox, project) = dse.setup(self.name, self.maxlength,
                                         seed,
                                         mp.cpu_count())
        self.project = project
        par, logbook = dse.run(toolbox, app, arch, popn=self.npop,
                               ngen=self.ngen, l=self.lambda_)
        logging.info("pareto front: %s" % par)
        self.assertEqual(ref_pareto, par, "pareto: %s" % par)

if __name__ == '__main__':
    # All tests
    #suite = unittest.TestLoader().loadTestsFromTestCase(TestMJPEG)

    # To select tests
    suite = unittest.TestSuite()
    suite.addTest(TestMJPEG('test_mjpeg1'))
    suite.addTest(TestMJPEG('test_mjpeg2'))
    suite.addTest(TestMJPEG('test_mjpeg3'))
    unittest.TextTestRunner(verbosity=2).run(suite)
