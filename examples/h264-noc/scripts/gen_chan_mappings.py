from sesamemap import archyml
from sesamemap import appyml
from pyyml import ymlmap

import lxml.objectify
import sys
import re

NAMESPACE = 'http://sesamesim.sourceforge.net/YML_Map'


if len(sys.argv) != 5:
    print "usage: %s <yml-app> <yml-arch> <yml-task-map> <yml-map-output>" % sys.argv[0]
    sys.exit(2)

# Notes
# read app, arch and map
# for each app channel c: A.p1 --> B.p2 :
#   X = map[A]
#   Y = map[B]
#   find correct networkinterface connected to X
#   (check if channel c is already mapped)
#   map c to correct networkinterface
# actually we don't need app and arch if we trust map.
# all info is available in map.yml:
# A->core1
# B->core2
# A-->B -> netinterfaceX
# We need to assume the numbering rule, and trust it:
# coreX -> NI (X-1), NI (X-1+CORES_NUMBER)


# parse yml files
app = appyml.Application(sys.argv[1])
arch = archyml.Architecture(sys.argv[2])
mapping = ymlmap.parse(sys.argv[3])

# TODO: extract from arch
NR_CORES = len(filter(lambda i: i.name.find("core") != -1, arch.components) )
print "Extracting number of cores: %d" % NR_CORES

# dest_map is where we add our mappings.
for maps in mapping.findall(
       ".//{%s}mapping[@name='architecture']" % NAMESPACE):
    dest_map = maps

# test some function in maplib
#print mapping.get_map('B').get_dest()
#print mapping.get_map('B').get_source()
#print mapping.get_map('B').get("dest")
#print mapping.get_map('B').get("dtmpl")

# info: add map with:
# dest_map.add_map(str(l), fifo_name, "vfifo")


# check app channels
print "Check link mappings"
for l in app.channels:
    # extract info
    prod, cons = l.get_nodes()
    prod_dest = mapping.get_map(prod.name).get_dest()
    #link_dest =  mapping.get_map(l).get_dest()
    #link_templ =  mapping.get_map(l).get("dtmpl")
    # print info
    print "Checking link: '%s'" % (l)
    print "Producer task '%s' mapped on '%s'" % (prod.name, prod_dest)
    # So link should be mapped onto correct connected to prod_dest
    #print "Link '%s' mapped on '%s' templ:'%s'" % (l, link_dest, link_templ)

    # extract id's and check rules
    #ni_id = int(re.match(r"\D+(\d+)$", link_dest).group(1))
    core_id = int(re.match(r"\D+(\d+)$", prod_dest).group(1))
    #print "ni id: %d, core id: %d" % (ni_id, core_id)
    ni_id = core_id - 1 + NR_CORES
    link_dest = 'networkInterface' + str(ni_id)

    # Check actual connection in arch yml
    neighbors = arch.neighbors(prod_dest)
    if link_dest in neighbors:
        print "Component connected"
    else:
        print "No connection found, exiting.."
        exit(2)

    print "Adding mapping from %s --> %s\n" % (l, link_dest)
    dest_map.add_map(str(l), link_dest, "vlocalmem")
    
    #print len(arch.components)
    #print arch.components

# Write mapping yml to file  
ymlmap.store_mapping(sys.argv[4], mapping)

