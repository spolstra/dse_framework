/***************************************************************************************
 * 	File: 	pps->c
 * 	Description: Implements picture parameter set FSM 
 * 	Author: Adarsha Rao
 * 	Copyright Information: Morphing Machines Pvt. (Ltd. IISc New Technology Incubation).
 * 						   All rights reserved.
 * 	Last Modified:    09-19-2008
 * 	Revision History: 
 * 		09-19-2008 	Adarsha 	Initial Version
 * ---------------------------------------------------------------------------------
 ***************************************************************************************/

#include "decoderCommon.h"

//extern FILE *tracefile;

void pictureParamSet(nalUnit *nal, PpsStruct *pps, dword *bitPosIn, dword *bitPosOut)
{
	unsigned short videoData;

	pps->picParamSetId = 0;
	pps->seqParamSetId = 0;
	pps->entropyCodingModeFlag = 0;
	pps->picOrderPresentFlag = 0;
	pps->numSliceGroupsMinus1 = 0;
	pps->sliceGroupMapType = 0;
	pps->numRefIdxL0ActiveMinus1 = 0; 
	pps->numRefIdxL1ActiveMinus1 = 0; 
	pps->weightedPredFlag = 0;
	pps->weigthedBiPredIdc = 0;
	pps->picInitQpMinus26 = 0;
	pps->picInitQsMinus26 = 0;
	pps->chromaQpIndexOffset = 0;
	pps->deblockingFilterControlPresentFlag = 0;
	pps->intraPredFlag = 0;
	pps->redundantPicCntPresentFlag = 0;

	videoData = getVideoData(nal,UE,0);
	pps->picParamSetId = videoData;

	videoData = getVideoData(nal,UE,0);
	pps->seqParamSetId = videoData;

	videoData = getVideoData(nal,FLC,2);
	pps->entropyCodingModeFlag = (videoData >> 15) & 0x01;
	pps->picOrderPresentFlag = (videoData >> 14) & 0x01;

	videoData = getVideoData(nal,UE,0);
	pps->numSliceGroupsMinus1 = videoData;

	videoData = getVideoData(nal,UE,0);
	pps->numRefIdxL0ActiveMinus1 = videoData;

	videoData = getVideoData(nal,UE,0);
	pps->numRefIdxL1ActiveMinus1 = videoData;

	videoData = getVideoData(nal,FLC,3);
	pps->weightedPredFlag = (videoData >> 15) & 0x01;
	pps->weigthedBiPredIdc = (videoData >> 14) & 0x03;

	videoData = getVideoData(nal,SE,0);
	pps->picInitQpMinus26 = videoData;

	videoData = getVideoData(nal,SE,0);
	pps->picInitQsMinus26 = videoData;

	videoData = getVideoData(nal,SE,0);
	pps->chromaQpIndexOffset = videoData;

	videoData = getVideoData(nal,FLC,3);
	pps->deblockingFilterControlPresentFlag = (videoData >> 15) & 0x01;
	pps->intraPredFlag = (videoData >> 14) & 0x01;
	pps->redundantPicCntPresentFlag = (videoData >> 13) & 0x01;

	//fprintf(tracefile,"picParamSetId = %d\n",pps->picParamSetId );
	//fprintf(tracefile,"seqParamSetId = %d\n",pps->seqParamSetId );
	//fprintf(tracefile,"intraPredFlag = %d\n",pps->intraPredFlag );
}

