/***************************************************************************************
 * 	File: 	idct.c
 * 	Description: Implements Inverse Descret Cosine Transform module 
 * 	Author: Adarsha Rao
 * 	Copyright Information: Morphing Machines Pvt. (Ltd. IISc New Technology Incubation).
 * 						   All rights reserved.
 * 	Last Modified:    09-19-2008
 * 	Revision History: 
 * 		09-19-2008 	Adarsha 	Initial Version
 * ---------------------------------------------------------------------------------
 ***************************************************************************************/

#include "decoderCommon.h"

void prepare_dc_chroma_block(int coeffs[4], block4x4 c)
{
		int i;
		for(i = 0; i<16; ++i)
			c[i/4][i%4] = 0;

		c[0][0] = coeffs[0];
		c[0][2] = coeffs[1];
		c[2][0] = coeffs[2];
		c[2][2] = coeffs[3];
}

void zigzagscan(int coeffs[16], block4x4 c, unsigned char ac_block)
{
	int i,j,index;
	unsigned char zigzagScanMatrix4x4[16] = {
											  0, 2, 3, 9, 
											  1, 4, 8, 10, 
											  5, 7, 11,14,
											  6,12, 13, 15 
											};

	for(i = 0; i<4; ++i)
		for(j = 0; j<4; ++j)
		{
			if(i == 0 && j == 0 && ac_block)
				c[i][j] = 0;
			else
			{
				index = 4*i + j;
				c[i][j] = coeffs[zigzagScanMatrix4x4[index - ac_block]];
			}
		}
}

unsigned short levelScale4x4(unsigned char m, unsigned char i, unsigned char j)
{

	unsigned char qMatrix = 16;
	static const unsigned char v[6][3]=   { {10, 16, 13},
		{11, 18, 14},
		{13, 20, 16},
		{14, 23, 18},
		{16, 25, 20},
		{18, 29, 23} };

	if(i%2 == 0 && j%2 == 0)
		return v[m][0] * qMatrix;
	else if(i%2 == 1 && j%2 == 1)
		return v[m][1] * qMatrix;
	else
		return v[m][2] * qMatrix;
}

////////////////////////////////////
void scale(const IDCTParams *params, block4x4 c, block4x4 out,
		YUV_TYPE yuv, unsigned char dc_block)
{
	int qP, m, qBits, f, i, j;
	int row, col, threshold;

	qP = (yuv == Y) ? params->qPy : params->qPc;
	threshold = (dc_block) ? 36 : 24;
	m = qP%6;

	if(dc_block)
	{
		qBits = (yuv == Y) ? MY_ABS(qP/6 , 6) : params->qPc/6;
		f = ((yuv == Y) && (params->qPy < 36)) ? 1 << (5 - params->qPy/6) : 0;
	}
	else
	{
		qBits = MY_ABS(qP/6 , 4);
		f = (qP < 24) ? 1 << (3 - qP/6) : 0;
	}
	
	// Luma DC scaling
	for(i = 0; i < 4; ++i)
		for(j = 0; j< 4; ++j)
		{
			row = (dc_block) ? 0 : i;
			col = (dc_block) ? 0 : j;

			if(dc_block && yuv != Y)
				out[i][j] = (((c[i][j] * levelScale4x4(m,0,0)) + f) << qBits) >> 5;

			else if(qP >= threshold)
				out[i][j] = (c[i][j] * levelScale4x4(m,row,col) + f ) << qBits;
			else
				out[i][j] = (c[i][j] * levelScale4x4(m,row,col) + f ) >> qBits;
		}
}

void transform4x4(block4x4 in, unsigned char dc_block, block4x4 out)
{
	int e00, e10, e20, e30; 
    int e01, e11, e21, e31; 
    int e02, e12, e22, e32; 
    int e03, e13, e23, e33;
	int tmp[4][4];
	short i,j;

	unsigned char shift = dc_block ? 0 : 1; 

	// stage1
			e00 = in[0][0] + in[2][0]; 
			e10 = in[0][1] + in[2][1];
			e20 = in[0][2] + in[2][2]; 	
			e30 = in[0][3] + in[2][3];
			e03 = in[1][0] + (in[3][0] >> shift);
			e13 = in[1][1] + (in[3][1] >> shift);
			e23 = in[1][2] + (in[3][2] >> shift);
			e33 = in[1][3] + (in[3][3] >> shift);

			e01 = in[0][0] - in[2][0]; 
			e11 = in[0][1] - in[2][1];
			e21 = in[0][2] - in[2][2]; 
			e31 = in[0][3] - in[2][3];
			e02 =(in[1][0] >> shift) - in[3][0]; 
			e12 =(in[1][1] >> shift) - in[3][1];
			e22 =(in[1][2] >> shift) - in[3][2]; 
			e32 =(in[1][3] >> shift) - in[3][3];

	// stage 2
			tmp[0][0] = e00 + e03;			tmp[3][0] = e00 - e03;		
			tmp[0][1] = e10 + e13;			tmp[3][1] = e10 - e13;
			tmp[0][2] = e20 + e23;			tmp[3][2] = e20 - e23;
			tmp[0][3] = e30 + e33;			tmp[3][3] = e30 - e33;		 
			
			tmp[1][0] = e01 + e02;			tmp[2][0] = e01 - e02;
			tmp[1][1] = e11 + e12;			tmp[2][1] = e11 - e12;
			tmp[1][2] = e21 + e22;			tmp[2][2] = e21 - e22;			
			tmp[1][3] = e31 + e32;			tmp[2][3] = e31 - e32;

	// stage 3
			e00 = tmp[0][0] + tmp[0][2]; 		e03 = tmp[3][0] + tmp[3][2];
			e10 = tmp[0][0] - tmp[0][2];       	e13 = tmp[3][0] - tmp[3][2];
			e20 = (tmp[0][1] >> shift) - tmp[0][3];	e23 = (tmp[3][1] >> shift) - tmp[3][3];
			e30 = tmp[0][1] + (tmp[0][3] >> shift);	e33 = tmp[3][1] + (tmp[3][3] >> shift);

			e01 = tmp[1][0] + tmp[1][2]; 		e02 = tmp[2][0] + tmp[2][2];
			e11 = tmp[1][0] - tmp[1][2]; 		e12 = tmp[2][0] - tmp[2][2];
			e21 = (tmp[1][1] >> shift) - tmp[1][3]; e22 = (tmp[2][1] >> shift) - tmp[2][3];
			e31 = tmp[1][1] + (tmp[1][3] >> shift); e32 = tmp[2][1] + (tmp[2][3] >> shift);

	// stage 4
			out[0][0] = e00 + e30;			out[3][0] = e03 + e33;
			out[0][1] = e10 + e20; 			out[3][1] = e13 + e23;
			out[0][2] = e10 - e20; 			out[3][2] = e13 - e23;
			out[0][3] = e00 - e30; 			out[3][3] = e03 - e33;

			out[2][0] = e02 + e32;			out[1][0] = e01 + e31;
			out[2][1] = e12 + e22;			out[1][1] = e11 + e21;
			out[2][2] = e12 - e22;			out[1][2] = e11 - e21;
			out[2][3] = e02 - e32;			out[1][3] = e01 - e31;

			if(!dc_block)
				for(i = 0; i<4 ; ++i)
					for(j = 0; j < 4; ++j)
						out[i][j] = (out[i][j] + 32) >> 6;
}

void Idct(const IDCTParams *params, const transformCoeffType *coeffs, MacroBlock *out)
{
	short i;
	block4x4 c, dc_block;

	// process Y
	if(params->i16MB)
	{
		zigzagscan((int *)coeffs->lumaDC, c, 0);
		transform4x4(c, 1, c);
		scale(params, c, dc_block, Y, 1);
	}

	for(i = 0; i<16; ++i)
	{
		zigzagscan((int *)coeffs->lumaAC[i], c, params->i16MB);
		if(params->i16MB)
			c[0][0] = dc_block[i/4][i%4];
		scale(params, c, c, Y, 0);
		transform4x4(c, 0, out->Y[i]);
	}
	
	// process U
	prepare_dc_chroma_block((int *)coeffs->chromaDCU, c);
	transform4x4(c, 1, c);
	scale(params, c, dc_block, U, 1);

	for(i = 0; i<4; ++i)
	{
		zigzagscan((int *)coeffs->chromaACU[i], c, 1);
		scale(params, c, c, U, 0);
		c[0][0] = dc_block[i/2][i%2];
		transform4x4(c, 0, out->U[i]);
	}

	// process V

	prepare_dc_chroma_block((int *)coeffs->chromaDCV,c);
	transform4x4(c,1,c);
	scale(params, c, dc_block, V, 1);

	for(i = 0; i<4; ++i)
	{
		zigzagscan((int *)coeffs->chromaACV[i], c, 1);
		scale(params, c, c, V, 0);
		c[0][0] = dc_block[i/2][i%2];
		transform4x4(c, 0, out->V[i]);
	}
}

