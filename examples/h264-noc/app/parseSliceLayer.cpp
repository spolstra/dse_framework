/***************************************************************************************
 * 	File: 	sliceLayer.c
 * 	Description: Implements slice Layer FSM
 * 	Author: Adarsha Rao
 * 	Copyright Information: Morphing Machines Pvt. (Ltd. IISc New Technology Incubation).
 * 						   All rights reserved.
 * 	Last Modified:    09-19-2008
 * 	Revision History: 
 * 		09-19-2008 	Adarsha 	Initial Version
 * ---------------------------------------------------------------------------------
 ***************************************************************************************/

#include "decoderCommon.h"

//extern FILE *tracefile;

void parseSliceHeader(const nalUnit *nal, 
                      const NalStruct *NalParams,
		      const SpsStruct *sps, 
		      const PpsStruct *pps, 
		      const dword *bitPosIn, 
		      ShStruct *sliceHeader, 
                      dword *bitPosOut, 
                      unsigned char* firstMbFlag)
{
	unsigned short videoData = 0;

	sliceHeader->firstMbInSlice 			= 0;
	sliceHeader->sliceType 				= (SLICE_TYPE)0;
	sliceHeader->picParameterSetId 			= 0;
	sliceHeader->frameNum 				= 0;
	sliceHeader->idrPicId 				= 0;
	sliceHeader->picOrderCntLsb 			= 0; 
	sliceHeader->picOrderCnt[0] 			= 0; 
	sliceHeader->picOrderCnt[1] 			= 0; 
	sliceHeader->redundantPicCnt 			= 0; 
	sliceHeader->directSpacialMvPredFlag		= 0; 
	sliceHeader->numRefIdxActiveOverrideFlag	= 0; 
	sliceHeader->numRefIdxL0ActiveMinus1 		= 0; 
	sliceHeader->numRefIdxL1ActiveMinus1 		= 0; 
	sliceHeader->cabacInitIdc  			= 0; 
	sliceHeader->sliceQpDelta  			= 0; 
	sliceHeader->disableDeblockingFilterIdc		= 0; 
	sliceHeader->sliceAlphaC0OffsetDiv2 		= 0 ;
	sliceHeader->sliceBetaOffsetDiv2  		= 0;
	sliceHeader->adaptiveRefPicMarkingModeFlag 	= 0;
	sliceHeader->refPicListReorderFlgL0 		= 0;
	sliceHeader->refPicListReorderFlgL1 		= 0;
	sliceHeader->reorderingOfPicNumIdc 		= 0;
	sliceHeader->absDiffPicNumMinus1 		= 0;
	sliceHeader->longTermPicNum 			= 0;
	sliceHeader->noOutputOfPriorPicsFlag 		= 0;
	sliceHeader->longTermReferenceFlag 		= 0;
	sliceHeader->memoryManagementControlOperation 	= 0;
	sliceHeader->differenceOfPicNumsMinus1 		= 0;
	sliceHeader->longTermFrameIdx 			= 0;
	sliceHeader->maxLongTermFrameIdxPlus1 		= 0;
    

	videoData = getVideoData((nalUnit *)nal,UE,0);
	sliceHeader->firstMbInSlice = videoData;

	videoData = getVideoData((nalUnit *)nal,UE,0);
	if(videoData == 0 || videoData == 5)
		sliceHeader->sliceType = P_SLICE;
	if(videoData == 1 || videoData == 6)
		sliceHeader->sliceType = B_SLICE;
	if(videoData == 2 || videoData == 7)
		sliceHeader->sliceType = I_SLICE;
	if(videoData == 3 || videoData == 8)
		sliceHeader->sliceType = SP_SLICE;
	if(videoData == 4 || videoData == 9)
		sliceHeader->sliceType = SI_SLICE;

	videoData = getVideoData((nalUnit *)nal,UE,0);
	sliceHeader->picParameterSetId = videoData;
	//fprintf(tracefile,"picParameterSetId = %d\n",sliceHeader->picParameterSetId );
	
	videoData = getVideoData((nalUnit *)nal,FLC,(sps->log2MaxFrameNumMinus4 + 4));
	sliceHeader->frameNum = videoData >> (16 - (sps->log2MaxFrameNumMinus4 + 4));
	//fprintf(tracefile,"frameNum = %d\n",sliceHeader->frameNum );

	if(NalParams->nalUnitType == 5)
	{
		videoData = getVideoData((nalUnit *)nal,UE,0);
		sliceHeader->idrPicId = videoData;
		//fprintf(tracefile,"idrPicId = %d\n",sliceHeader->idrPicId);
	}

	videoData = getVideoData((nalUnit *)nal,FLC,(sps->log2MaxPicOrderCntLsbMinus4 + 4));
	sliceHeader->picOrderCntLsb = videoData >> (16 - (sps->log2MaxPicOrderCntLsbMinus4 + 4));
	//fprintf(tracefile,"picOrderCntLsb = %d\n",sliceHeader->picOrderCntLsb );
		
	if(NalParams->nalRefIdc != 0)
		decRefPicMarking((nalUnit *)nal, (NalStruct *)NalParams, sliceHeader, (dword *)bitPosIn, bitPosOut);

	videoData = getVideoData((nalUnit *)nal,SE,0);
	sliceHeader->sliceQpDelta = videoData;
	sliceHeader->sliceQp = 26 + pps->picInitQpMinus26 + sliceHeader->sliceQpDelta;
	//fprintf(tracefile,"sliceQpDelta = %d\n",sliceHeader->sliceQpDelta );

	*firstMbFlag = 1;
}

void refPicListReordering(nalUnit *nal, ShStruct *sliceHeader, dword *bitPosIn, dword *bitPosOut)
{
}
	
void decRefPicMarking(nalUnit *nal, NalStruct *NalParams, ShStruct *sliceHeader, dword *bitPosIn, dword *bitPosOut)
{
	unsigned short videoData;

	if(NalParams->nalUnitType == 5)
	{
		videoData = getVideoData(nal,FLC,2);
		sliceHeader->noOutputOfPriorPicsFlag = (videoData >> 15) & 0x01;
		//fprintf(tracefile,"noOutputOfPriorPicsFlag = %d\n",sliceHeader->noOutputOfPriorPicsFlag );
		sliceHeader->longTermReferenceFlag = (videoData >> 14) & 0x01;
		//fprintf(tracefile,"longTermReferenceFlag = %d\n",sliceHeader->longTermReferenceFlag );
	}
	else
	{
		videoData = getVideoData(nal,FLC,1);
		sliceHeader->adaptiveRefPicMarkingModeFlag = (videoData >> 15) & 0x01;
		//fprintf(tracefile,"adaptiveRefPicMarkingModeFlag = %d\n",sliceHeader->adaptiveRefPicMarkingModeFlag );
	}
}

void init_mb(SpsStruct *const sps, PpsStruct *const pps, ShStruct * const sliceHeader,
		MblStruct *Mbl)
{
	unsigned char prevMbSkipped;

	Mbl->mbNum = sliceHeader->firstMbInSlice;
	Mbl->leftAvail = 0;
	Mbl->topAvail = 0;
	Mbl->mbAddrA = 0;
	Mbl->mbAddrB = 0;
	Mbl->mb_y = Mbl->mbNum / sps->picWidthInMbs;
	Mbl->mb_x = Mbl->mbNum % sps->picWidthInMbs;

	Mbl->qPy = sliceHeader->sliceQp;
	Mbl->qPc = sliceHeader->sliceQp + pps->chromaQpIndexOffset;

	prevMbSkipped = 0;
	Mbl->skipped = 0;

		Mbl->leftInter = 1;
		Mbl->topInter =  1;
		Mbl->leftIntra4x4 = 0;
		Mbl->topIntra4x4 = 0;
		Mbl->cbpLumaLeft = 0;
		Mbl->cbpLumaTop = 0;
		Mbl->cbpChromaLeft = 0;
		Mbl->cbpChromaTop =  0;
		Mbl->qPyLeft = 0;
		Mbl->qPyTop =  0;
		Mbl->qPcLeft = 0;
		Mbl->qPcTop =  0;
}


