#include "decoderCommon.h"

static block4x4 lby[4];
static block4x4 tby[256];
static block4x4 lbu[2];
static block4x4 lbv[2];
static block4x4 tbu[128];
static block4x4 tbv[128];

void filterCore(filterParams params, pixelRow *x, pixelRow *op) 
{
	int qPav, indexA, indexB;
	int alpha, beta, tc0Prime, ap, aq, tc0;
	unsigned char apLtBeta, aqLtBeta, delta, cond_492, cond_485, cond_479, cond_481, filterSampleFlag;

	int temp1, temp, diff1, diff2, diff3;

	static unsigned char alphaTable[52] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
										   4,4,5,6,7,8,9,10,12,13,15,17,20,22,25,28,
										   32,36,40,45,50,56,63,71,80,90,101,113,127,144,162,182,
										   203,226,255,255};
	static unsigned char betaTable[52] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
										   2,2,2,3,3,3,3,4,4,4,6,6,7,7,8,8,
										   9,9,10,10,11,11,12,12,13,13,14,14,15,15,16,16,
										   17,17,18,18};
	static unsigned char tcTable[3][52] = { { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
											  0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,
											  1,2,2,2,2,3,3,3,4,4,4,5,6,6,7,8,
											  9,10,11,13},
						 					{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
											  0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,2,
											  2,2,2,3,3,3,4,4,5,5,6,7,8,8,10,11,
											  12,13,15,17},
											{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
											  0,1,1,1,1,1,1,1,1,1,1,2,2,2,2,3,
											  3,3,4,4,4,5,6,6,7,8,9,10,11,13,14,16,
											  18,20,23,25}
											};
	qPav = (params.qP + params.qPLeft + 1) >> 1;

	indexA = Clip3(0, 51, qPav + params.filterOffsetA);
	indexB = Clip3(0, 51, qPav + params.filterOffsetB);

	alpha = alphaTable[indexA];
	beta = betaTable[indexB];
	
	if( params.BS == 1 || params.BS == 2 || params.BS == 3)
		tc0Prime = tcTable[params.BS - 1][indexA];
	else
		tc0Prime = 0;


	diff1 = MY_ABS(x->q[1],x->q[0]);
	diff2 = MY_ABS(x->p[1],x->p[0]);
	diff3 = MY_ABS(x->p[0],x->q[0]);

	ap = MY_ABS(x->p[2] , x->p[0]);
	aq = MY_ABS(x->q[2] , x->q[0]);
	apLtBeta = (ap < beta);
	aqLtBeta = (aq < beta);
		
	temp1 = (beta > diff1) && ( beta > diff2);
	temp = ((params.BS != 0) && (diff3 < alpha) && temp1);

	filterSampleFlag = (params.leftAvailable && temp) ? 1 : 0;

	if(params.BS < 4)
		if(params.chromaEdgeFlag == 0)
			tc0 = tc0Prime + ((apLtBeta) ? 1:0) + ((aqLtBeta) ? 1:0);
		else
			tc0 = tc0Prime + 1;
	else
		tc0 = 0;

	delta = Clip3(-tc0, tc0, ((((x->q[0] - x->p[0])<<2) + (x->p[1] - x->q[1]) + 4) >> 3));
	cond_479 = (!params.chromaEdgeFlag && apLtBeta);
	cond_481 = (!params.chromaEdgeFlag && aqLtBeta);
	cond_485 = (!params.chromaEdgeFlag && apLtBeta && MY_ABS(x->p[0],x->q[0])<((alpha>>2) + 2));
	cond_492 = (!params.chromaEdgeFlag && apLtBeta && MY_ABS(x->p[0],x->q[0])<((alpha>>2) + 2));

	filterSampleFlag = 0;
	if(filterSampleFlag)
	{
		if(params.BS < 4)
		{

			op->p[0] = Clip1(x->p[0] + delta);
			op->q[0] = Clip1(x->q[0] - delta);
			if(cond_479)
			{
				short t1, t2, t3;

				t1 = ((x->p[0] + x->q[0] + 1)>>1);
				t2 = Clip3(-tc0, tc0, (x->p[2] + t1 - (x->p[1] << 1)) >> 1);
				t3 = Clip3(-tc0, tc0, (x->q[2] + t1 - (x->q[1] << 1)) >> 1);
				op->p[1] = x->p[1] + t2;
				op->q[1] = x->q[1] + t3;
			}
			else
			{
				op->p[1] = x->p[1];
				op->q[1] = x->q[1];
			}
			op->p[2] = x->p[2];
			op->q[2] = x->q[2];
		}
		else if( params.BS == 4)
		{
			if(cond_485)
			{
				op->p[0] = (x->p[2] + 2*x->p[1] + 2*x->p[0] + 2*x->q[0] + x->q[1] + 4) >> 3;
				op->p[1] = (x->p[2] + x->p[1] + x->p[0] + x->q[0] + 2) >> 2;
				op->p[2] = (2*x->p[3] + 3*x->p[2] + x->p[1] + x->p[0] + x->q[0] + 4) >> 3;
			}
			else
			{
				op->p[0] = (2*x->p[1] + x->p[0] + x->q[1] + 2) >> 2;
				op->p[1] = x->p[1];
				op->p[2] = x->p[2];
			}

			if(cond_492)
			{
				op->q[0] = (x->p[1] + 2*x->p[0] + 2*x->q[0] + 2*x->q[1] + x->q[2] + 4) >> 3;
				op->q[1] = (x->q[2] + x->q[1] + x->p[0] + x->q[0] + 2) >> 2;
				op->q[2] = (2*x->q[3] + 3*x->q[2] + x->q[1] + x->q[0] + x->p[0] + 4) >> 3;
			}
			else
			{
				op->q[0] = (2*x->q[1] + x->q[0] + x->p[1] + 2) >> 2;
				op->q[1] = x->q[1];
				op->q[2] = x->q[2];
			}
		}
		op->p[3] = x->p[3];
		op->q[3] = x->q[3];
	}
	else
	{
		op->p[0] = x->p[0];
		op->p[1] = x->p[1];
		op->p[2] = x->p[2];
		op->p[3] = x->p[3];

		op->q[0] = x->q[0];
		op->q[1] = x->q[1];
		op->q[2] = x->q[2];
		op->q[3] = x->q[3];
	}
}

void filterPipeline(filterParams params, block4x4 p, block4x4 q, 
		block4x4 pprime, block4x4 qprime)
{
	int rowNum = 0;
	pixelRow ip, op;

	for(rowNum = 0; rowNum < 4; ++rowNum)
	{
		ip.p[0] = p[rowNum][3]; ip.p[1] = p[rowNum][2]; 
		ip.p[2] = p[rowNum][1]; ip.p[3] = p[rowNum][0]; 

		ip.q[0] = q[rowNum][0]; ip.q[1] = q[rowNum][1]; 
		ip.q[2] = q[rowNum][2]; ip.q[3] = q[rowNum][3]; 
		
		filterCore(params, &ip, &op);

		pprime[rowNum][0] = op.p[3]; pprime[rowNum][1] = op.p[2]; 
		pprime[rowNum][2] = op.p[1]; pprime[rowNum][3] = op.p[0]; 
                           
		qprime[rowNum][0] = op.q[0]; qprime[rowNum][1] = op.q[1]; 
		qprime[rowNum][2] = op.q[2]; qprime[rowNum][3] = op.q[3]; 
	}
}

void transpose(block4x4 x)
{
	int i, j;
	short temp;
	for(i = 0; i< 4; ++i)
		for(j = i+1; j <4; ++j)
		{
			temp = x[i][j];
			x[i][j] = x[j][i];
			x[j][i] = temp;
		}
}

void copy_block(const block4x4 x, block4x4 y)
{
	int i, j;
	for(i = 0; i< 4; ++i)
		for(j = 0; j <4; ++j)
			y[i][j] = x[i][j];
}

void lumaFilter(dbParams *params, MacroBlock *ip_mb, MacroBlock * op_mb, 
		block4x4 *lb, block4x4 *tb, int dir)
{
	int hfilter_luma_order[] = {0,1,4,5,2,3,6,7,8,9,12,13,10,11,14,15};
	int vfilter_luma_order[] = {0,2,8,10,1,3,9,11,4,6,12,14,5,7,13,15};
	filterParams fIps; 
	block4x4 *p, *q;
	block4x4 *pprime, *qprime;
	int edge_num;
	int block_num;

	fIps.qP = params->qPy; 
	fIps.filterOffsetA = params->filterOffsetA; 
	fIps.filterOffsetB = params->filterOffsetB; 

	fIps.chromaEdgeFlag = 0;

	for(edge_num = 0 ;edge_num < 4; ++edge_num)
	{
		if( edge_num == 0 )
		{
			if(dir == 1)
			{
				fIps.leftAvailable = params->topAvail || (edge_num != 0);
				fIps.qPLeft = params->qPyTop; 
			}
			else
			{
				fIps.leftAvailable = params->leftAvail || (edge_num != 0);
				fIps.qPLeft = params->qPyLeft; 
			}
			fIps.BS = 4; 
		}
		else
		{
			fIps.leftAvailable = 1;
			fIps.qPLeft = params->qPy; 
			fIps.BS = 3; 
		}

		for(block_num = 0; block_num < 4; ++ block_num)
		{
			if(dir == 1)
			{
				q = &ip_mb->Y[hfilter_luma_order[edge_num*4+block_num]];
				qprime = &op_mb->Y[hfilter_luma_order[edge_num*4+block_num]];
			}
			else
			{
				q = &ip_mb->Y[vfilter_luma_order[edge_num*4+block_num]];
				qprime = &op_mb->Y[vfilter_luma_order[edge_num*4+block_num]];
			}
			if(dir == 1)
			{
				p = (edge_num == 0) ? &tb[params->mb_x*4+block_num] : 
							&ip_mb->Y[hfilter_luma_order[(edge_num-1)*4+block_num]];
				pprime = (edge_num == 0) ? &tb[params->mb_x*4+block_num] : 
							&op_mb->Y[hfilter_luma_order[(edge_num-1)*4+block_num]];
			}
			else
			{
				p = (edge_num == 0) ? &lb[block_num] : 
							&ip_mb->Y[vfilter_luma_order[(edge_num-1)*4+block_num]];
				pprime = (edge_num == 0) ? &lb[block_num] : 
							&op_mb->Y[vfilter_luma_order[(edge_num-1)*4+block_num]];
			}
			if(dir == 1)
			{
				transpose(*p);
				transpose(*q);
			}
			filterPipeline(fIps, *p, *q, *pprime, *qprime);
			if(dir == 1)
			{
				//transpose(*pprime);
				//transpose(*qprime);
			}
		}
	}
}

void chromaFilter(dbParams *const params, MacroBlock *ip_mb, MacroBlock *op_mb, 
		block4x4 *lb, block4x4 *tb, int dir, int uv)
{

	int vfilter_chroma_order[] = {0,2,1,3};
	int hfilter_chroma_order[] = {0,1,2,3};
	filterParams fIps; 
	block4x4 *p, *q;
	block4x4 *pprime, *qprime;
	int edge_num;
	int block_num;

	fIps.qP = params->qPc; 
	fIps.filterOffsetA = params->filterOffsetA; 
	fIps.filterOffsetB = params->filterOffsetB; 

	fIps.chromaEdgeFlag = 1;

	for(edge_num = 0;edge_num < 2; ++edge_num)
	{
		if( edge_num == 0 )
		{
			if(dir == 1)
			{
				fIps.leftAvailable = params->topAvail;
				fIps.qPLeft = params->qPcTop; 
			}
			else
			{
				fIps.leftAvailable = params->leftAvail;
				fIps.qPLeft = params->qPcLeft; 
			}
			fIps.BS = 4; 
		}
		else
		{
			fIps.leftAvailable = 1;
			fIps.qPLeft = params->qPc; 
			fIps.BS = 3; 
		}

		for(block_num = 0; block_num < 2; ++ block_num)
		{
			if(dir == 1)
			{
				if(uv == 0)
				{
					//q = &ip_mb->U[hfilter_chroma_order[edge_num*2+block_num]];
					q = &(*ip_mb).U[hfilter_chroma_order[edge_num*2+block_num]];

					qprime = &op_mb->U[hfilter_chroma_order[edge_num*2+block_num]];
				}
				else
				{
					q = &ip_mb->V[hfilter_chroma_order[edge_num*2+block_num]];
					qprime = &op_mb->V[hfilter_chroma_order[edge_num*2+block_num]];
				}
			}
			else
			{
				if(uv == 0)
				{
					q = &ip_mb->U[vfilter_chroma_order[edge_num*2+block_num]];
					qprime = &op_mb->U[vfilter_chroma_order[edge_num*2+block_num]];
				}
				else
				{
					q = &ip_mb->V[vfilter_chroma_order[edge_num*2+block_num]];
					qprime = &op_mb->V[vfilter_chroma_order[edge_num*2+block_num]];
				}
			}


			if(dir == 1)
			{
				if(uv == 0)
				{
					p = (edge_num == 0) ? &tb[params->mb_x*2+block_num] : 
								&ip_mb->U[hfilter_chroma_order[block_num]];
					pprime = (edge_num == 0) ? &tb[params->mb_x*2+block_num] : 
								&op_mb->U[hfilter_chroma_order[block_num]];
				}
				else
				{
					p = (edge_num == 0) ? &tb[params->mb_x*2+block_num] : 
								&ip_mb->V[hfilter_chroma_order[block_num]];
					pprime = (edge_num == 0) ? &tb[params->mb_x*2+block_num] : 
								&op_mb->V[hfilter_chroma_order[block_num]];
				}
			}
			else
			{
				if(uv == 0)
				{
					p = (edge_num == 0) ? &lb[block_num] : 
								&ip_mb->U[vfilter_chroma_order[block_num]];
					pprime = (edge_num == 0) ? &lb[block_num] : 
								&op_mb->U[vfilter_chroma_order[block_num]];
				}
				else
				{
					p = (edge_num == 0) ? &lb[block_num] : 
								&ip_mb->V[vfilter_chroma_order[block_num]];
					pprime = (edge_num == 0) ? &lb[block_num] : 
								&op_mb->V[vfilter_chroma_order[block_num]];
				}
			}

			if(dir == 1)
			{
				transpose(*p);
				transpose(*q);
			}

			filterPipeline(fIps, *p, *q, *pprime, *qprime);

		//	if(dir == 1)
		//	{
		//		transpose(*pprime);
		//		transpose(*qprime);
		//	}
		}
	}
}

void storeContext(dbParams *params, MacroBlock *op_mb, 
		block4x4 *lby, block4x4 *tby, block4x4 *lbu, block4x4 *tbu, 
		block4x4 *lbv, block4x4 *tbv)
{
	copy_block(op_mb->Y[5], lby[0]);
	copy_block(op_mb->Y[7], lby[1]);
	copy_block(op_mb->Y[13], lby[2]);
	copy_block(op_mb->Y[15], lby[3]);

	copy_block(op_mb->Y[10], tby[params->mb_x*4]);
	copy_block(op_mb->Y[11], tby[params->mb_x*4 + 1]);
	copy_block(op_mb->Y[14], tby[params->mb_x*4 + 2]);
	copy_block(op_mb->Y[15], tby[params->mb_x*4 + 3]);

	copy_block(op_mb->U[1], lbu[0]);
	copy_block(op_mb->U[3], lbu[1]);

	copy_block(op_mb->U[2], tbu[params->mb_x*2]);
	copy_block(op_mb->U[3], tbu[params->mb_x*2 + 1]);

	copy_block(op_mb->V[1], lbv[0]);
	copy_block(op_mb->V[3], lbv[1]);

	copy_block(op_mb->V[2], tbv[params->mb_x*2]);
	copy_block(op_mb->V[3], tbv[params->mb_x*2 + 1]);
}

void deblocking_filter(const dbParams *params, const MacroBlock *ip_mb, MacroBlock *op_mb)
{
	lumaFilter((dbParams *)params, (MacroBlock *)ip_mb, op_mb, lby, tby, 0);
	lumaFilter((dbParams *)params, (MacroBlock *)ip_mb, op_mb, lby, tby, 1);

	chromaFilter((dbParams *)params, (MacroBlock *)ip_mb, op_mb, lbu, tbu, 0, 0);
	//chromaFilter(params, ip_mb, op_mb, lbu, tbu, 1, 0);

	chromaFilter((dbParams *)params, (MacroBlock *)ip_mb, op_mb, lbu, tbu, 0, 1);
	//chromaFilter(params, ip_mb, op_mb, lbu, tbu, 1, 1);
		
	storeContext((dbParams *)params, op_mb, lby, tby, lbu, tbu, lbv, tbv);
	//clip_mb(op_mb);
}

void clip_mb(MacroBlock *mb)
{
	int i, j, k;

	for(i = 0; i< 16; ++i)
		for(j = 0; j< 4; ++j)
			for(k = 0; k<4; ++k)
				mb->Y[i][j][k] = (mb->Y[i][j][k] < 0) ? 0 : ((mb->Y[i][j][k] > 255)? 255 : mb->Y[i][j][k]);

	for(i = 0; i< 4; ++i)
		for(j = 0; j< 4; ++j)
			for(k = 0; k<4; ++k)
				mb->U[i][j][k] = (mb->U[i][j][k] < 0) ? 0 : ((mb->U[i][j][k] > 255)? 255 : mb->U[i][j][k]);

	for(i = 0; i< 4; ++i)
		for(j = 0; j< 4; ++j)
			for(k = 0; k<4; ++k)
				mb->Y[i][j][k] = (mb->U[i][j][k] < 0) ? 0 : ((mb->U[i][j][k] > 255)? 255 : mb->U[i][j][k]);
}

