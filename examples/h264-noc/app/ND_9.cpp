// File automatically generated by ESPAM

#include "ND_9.h"

ND_9::ND_9(Id n, ND_9_Ports *ports) : ND_9_Base(n, ports) {}
ND_9::~ND_9() {}

void ND_9::_printMB( struct MacroBlock in_0 ) {
    printMB( &in_0 );
}


void ND_9::main() {

    // Input Arguments 
    struct MacroBlock in_0ND_9;

    // Output Arguments 

    for( int c0 =  ceil1(0); c0 <=  floor1(9); c0 += 1 ) {
      for( int c1 =  ceil1(0); c1 <=  floor1(98); c1 += 1 ) {

          ports->IG_1.read( in_0ND_9 );


          _printMB(in_0ND_9) ;
          execute("op_printMB");

      } // for c1
    } // for c0
} // main
