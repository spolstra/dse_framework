import unittest
import sys
import os
import logging
import shutil
import multiprocessing as mp

from dseframework import dse, dsehelpers

class TestH264(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # Need to change to script directory to run testcase
        cwd = os.path.dirname(os.path.abspath(__file__)) + '/'
        os.chdir(cwd)
        logging.basicConfig(level=logging.WARNING)

    def setUp(self):
        # Experiment parameters.
        shutil.copyfile('h264Decoder_map.yml-ref', 'h264Decoder_map.yml')
        self.name = "h264Decoder.yml"
        self.maxlength = sys.maxint
        self.npop = 20
        self.lambda_ = 20
        self.ngen = 4

    def tearDown(self):
        self.project.clean()

    def test_h264_noc1(self):
        # Random seed and matching reference pareto front.
        seed = 42
        ref_pareto = [(4976889.0, 440.0), (5358105.0, 330.0),
                      (7961064.0, 220.0)]
        self.run_and_compare(seed, ref_pareto)

    def test_h264_noc2(self):
        # Random seed and matching reference pareto front.
        seed = 1
        ref_pareto = [(4503603.0, 330.0)]
        self.run_and_compare(seed, ref_pareto)

    def test_h264_noc3(self):
        # Random seed and matching reference pareto front.
        seed = 100
        ref_pareto = [(1145838.0, 330.0), (6439717.0, 320.0)]
        self.run_and_compare(seed, ref_pareto)

    def run_and_compare(self, seed, ref_pareto):
        (app, arch, toolbox, project) = dse.setup(self.name, self.maxlength,
                                         seed,
                                         mp.cpu_count())
        self.project = project
        par, logbook = dse.run(toolbox, app, arch, popn=self.npop,
                               ngen=self.ngen, l=self.lambda_)
        logging.info("pareto front: %s" % par)
        self.assertEqual(ref_pareto, par, "pareto: %s" % par)

if __name__ == '__main__':
    # All tests
    #suite = unittest.TestLoader().loadTestsFromTestCase(TestH264)

    # To select tests
    suite = unittest.TestSuite()
    suite.addTest(TestH264('test_h264_noc1'))
    suite.addTest(TestH264('test_h264_noc2'))
    suite.addTest(TestH264('test_h264_noc3'))
    unittest.TextTestRunner(verbosity=2).run(suite)
