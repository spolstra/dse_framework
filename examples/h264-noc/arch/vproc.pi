#include "debug.h"
#include "operations.h"

class vproc

arch        : processor
nvchannels  : integer
vchannels_t = [nvchannels] vchannel
vchannels   : vchannels_t

ch_map   : integer = intmap_create()
vch_map  : integer = intmap_create()

size_init_t = [nvchannels] boolean
size_init : size_init_t

/* Empty callback function, used as room available signal */
room:() {}

statistics:() {}

{
    i     : integer
    vchid : integer
    chid  : integer
    ttype : integer
    proc_id: integer
    ch    : vchannel

    VP(VINIT,("%s-signing in as %d\n", whoami(),this));

    // This initiates virtual channel negotiation which allows
    // processors to identify virtual channels with actual hardware
    // communication paths with out port mapping information.
    for (i = 0; i < nvchannels; i += 1) {
        // initialise size init to false, so we know when to set it
        size_init[i] = false;

        // Get the virtual channel id
        vchid = vchannels[i] ! get_vchannel_id();
        proc_id=arch ! get_id();

        // SP: What does this function do Roberta? (comment here)
        // register_processor(vchid,proc_id);

        // SP: Cannot use this method any more. To many components between
        // source and destination. The arch proc will send all reads and
        // writes to its local memory. (default route?)
        // Sync messages will be handled seperately.

        // Not asking proc. vchannel got this info via proc-ni so we 
        // use it directly
        chid = vchannels[i] ! get_channel_id();
        VP(VINIT,("%s-init i:%d vchid:%d chid:%d\n", whoami(),i,vchid,chid));

        // Store this information
        intmap_put(vch_map, vchid, i);
        intmap_put(ch_map, vchid, chid);
    };
    VP(VINIT,("%s-init comm done %d\n", whoami(),this));

    // Start processing trace events
    while (true) {
        ttype = nextTrace();

        if (ttype == READ || ttype == WRITE) {
            // Look up channel id
            ch = vchannels[intmap_get(vch_map, traceCommID())];

            assert(traceSize() > 0, ("%s tracesize is:%d tracecomm:%d ttype:%d\n", whoami(), traceSize(), traceCommID(), ttype));
            // doesn't if read or write inits the token size of the channel
            // It will now be set twice to the same value, from
            // producer and consumer side. This is not a problem.
            if ( size_init[intmap_get(vch_map, traceCommID())] == false) {
                ch ! set_token_size(traceSize());
                VP(VSPEC,("%s ttype:%d set size ch:%d size:%d\n", whoami(), ttype, ch, traceSize()));
                size_init[intmap_get(vch_map, traceCommID())] = true;
            };

            // would be index in proc array of connections, but for
            // the noc its the noc id of the sending processor.
            chid = intmap_get(ch_map, traceCommID());
        };
        switch (ttype) {
            READ {
                VP(VGEN,("%s read tracecomm id: %d\n", whoami(), traceCommID()));

                /* Checking fifo status with get_read_fill() takes time on
                 * the processor */
                arch ! execute_lat(FIFO_CHECK_EMPTY);
                if ( ch ! get_read_fill() == 0) {
                    // Need to send out a send_req
                    // pass vchannel id (ch) and destination index (chid)
                    // FIXIT
                    VP(VSPEC,("%s send_req: vchan:%d chid:%d\n", whoami(), ch, chid));
                    arch ! send_request(ch, chid);
                    // sync call to processer should generate async call across
                    // noc
                };
                ch ! check_data(); // send_request is send if read_fill was 0.
                // data should arrive 
                arch ! read(chid, traceCommID(), traceSize());
                ch ! signal_room();
            }

            WRITE {
                VP(VGEN,("%s Handling write event (%d)\n", whoami(), traceCommID()));
                // We block the processor, not just the vproc.
                arch ! execute_lat(FIFO_CHECK_FULL);
                ch !! check_room(this);
                VP(VGEN,("%s write after from check_room (%d)\n", whoami(), traceCommID()));
                block(room);
                VP(VGEN,("%s write after room callback\n", whoami()));

                arch ! write(chid, traceCommID(), traceSize());
                VP(VGEN,("%s write return from write (%d)\n", whoami(), traceCommID()));
                ch ! signal_data();
            }

            EXECUTE {
                arch ! execute(traceInstruction());
            }

            QUIT {break;}
        }
    }
}

