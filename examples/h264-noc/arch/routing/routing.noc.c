#include <stdio.h>
#include <assert.h>

// test code
int route_test (int x) {
    return x + 10;
}

/* routing functions, by Roberta */

/* obsolete function */
void register_processor (int vchannel_id, int proc_id){
    FILE *out_file;
    char str[10];


    const char base[] = ".txt";
    char filename [40];

    sprintf(filename, "%d%s",vchannel_id,base);

    out_file = fopen(filename, "a");

    fprintf (out_file, " %d \n",proc_id);

}

/* obsolete function */
int define_destination(int proc_id, int vchid ){
    int t=0;
    int value;
    const char base[] = ".txt";
    char filename [40];
    int final_dest=0;
    int dest;
    FILE *out_file;
    out_file=fopen(filename,"r");
    fscanf(out_file,"%d",&value);
    if(value!=proc_id){
        final_dest=value;
    }
    return final_dest;

}

// Route encoding: first hop in the LSBs, last hop in the MSBs
//unsigned int route_packets_c (unsigned int src_ID, unsigned int dest_ID){
int define_routing (int src_ID, int dest_ID){

    switch (src_ID){
        case 0x10 : 
            switch (dest_ID){
                case 0x4 : 		return 0x0 ; 
                case 0x12 : 		return 0x6 ; 
                case 0x13 :		return 0x6 ; 
                case 0x16 : 		return 0x7 ; 
                case 0x17 :		return 0x7 ; 
                default:	   	return 0; 
            }
        case 0x12 : 
            switch (dest_ID){
                case 0x5 : 		return 0x0 ; 
                case 0x10 : 		return 0x6 ; 
                case 0x11 :		return 0x6 ; 
                default:	   	return 0; 
            }
        case 0x14 : 
            switch (dest_ID){
                case 0x6 : 		return 0x0 ; 
                case 0x16 : 		return 0x6 ; 
                case 0x17 :		return 0x6 ; 
                default:	   	return 0; 
            }
        case 0x16 : 
            switch (dest_ID){
                case 0x7 : 		return 0x0 ; 
                case 0x10 : 		return 0x6 ; 
                case 0x11 :		return 0x6 ; 
                case 0x14 : 		return 0x7 ; 
                case 0x15 :		return 0x7 ; 
                default:	   	return 0; 
            }
        default:	return 0; 
    }
}

int port_select(int route, int mask){
    int lastbits = route & mask;
    return lastbits;
}

int update_route(int route, int shift){
    route = route >> shift;
    return route;
}
/* End routing functions */
