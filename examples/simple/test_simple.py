import unittest
import sys
import os
import logging
import multiprocessing as mp

from dseframework import dse, dsehelpers

class TestSimple(unittest.TestCase):

    def setUp(self):
        # Need to execute from simple project directory.
        os.chdir("simple")
        logging.basicConfig(level=logging.INFO)

        # We need the whole diff.
        self.maxDiff = None

        self.name = "simple.yml"
        self.maxlength = sys.maxint

        # Reference pareto front
        self.ref_pareto = [(210.0, 27.0), (210.0, 27.0),
                           (240.0, 22.0), (240.0, 22.0),
                           (300.0, 10.0), (300.0, 10.0)]

    def test_pareto(self):
        (app, arch, toolbox) = dse.setup(self.name, self.maxlength, 42,
                                         mp.cpu_count())
        par = dse.run(toolbox, app, arch)
        print "pareto front: %s" % par
        self.assertEqual(self.ref_pareto, par)
