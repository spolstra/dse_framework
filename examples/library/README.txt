These components are a good starting point to build a platform for the Aloha
project.
(initial version taken from bvlc_alexnet_BB_40)

Features:
* Processor checks the operindx for overflow in the execute method.
* channel size is read from the virtual layer yml file instead of
  requested from an arch component for fifo's.
  TODO: testcase for this.
* PANIC_IF_TOKENS in channels. vself checks for this. Other vchannels always
  panic.
* verbosity selection
