#/usr/bin/env bash

for m in $(ls simple_map_*.yml); do
    echo $m
    ln -fs $m simple_map.yml && make run | egrep 'Elaps|Used'
    echo
done
