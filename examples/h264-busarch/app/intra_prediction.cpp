#include "decoderCommon.h"

int min(int a, int b)
{
	return (a<b)?a:b;
}

int max(int a, int b)
{
	return (a>b)?a:b;
}

void get_pixels(blockParams *params, pixelStore* int_mem,
		int leftPixels[16], int topPixels[16], int *topleftPixel)
{
	int i;
	int xoff, yoff;

	if(params->yuv == Y)
	{
		if(params->i16MB)
		{
			xoff = params->mb_x*16;
			yoff = 0;
			for(i = 0; i<16; ++i)
			{
				leftPixels[i] = params->left_avail ? int_mem->leftPixelsY[yoff+i] : 128;
				topPixels[i] = params->up_avail ? int_mem->topPixelsY[xoff+i] : 128;
			}
			*topleftPixel = (params->left_avail && params->up_avail) ? int_mem->topLeftPixelY : 128;
		}
		else
		{
			xoff = params->mb_x*16 + (((params->block_num/4)%2)*2 + (params->block_num%4)%2)*4;
			yoff = (((params->block_num/4)/2)*2 + (params->block_num%4)/2 )*4;

			for(i = 0; i<4; ++i)
				topPixels[i] = (params->block_available_up) ? int_mem->topPixelsY[xoff+i]:128;
			for(i = 4; i<8; ++i)
				topPixels[i] = (params->block_available_up_right) ? int_mem->topPixelsY[xoff+i]:128;

			for(i = 0; i<4; ++i)
				leftPixels[i] = (params->block_available_left) ? int_mem->leftPixelsY[yoff+i] : 128;

			*topleftPixel = (params->block_available_up_left) ? int_mem->topLeftPixelY:128;
		}
	}
	else if(params->yuv == U)
	{
		xoff = params->mb_x*8 + (((params->block_num/4)%2)*2 + (params->block_num%4)%2)*4;
		yoff = (((params->block_num/4)/2)*2 + (params->block_num%4)/2 )*4;

		for(i = 0; i<4; ++i)
			topPixels[i] = (params->block_available_up) ? int_mem->topPixelsCb[xoff+i]:128;
		for(i = 0; i<4; ++i)
			leftPixels[i] = (params->block_available_left) ? int_mem->leftPixelsCb[yoff+i]:128;

		*topleftPixel = (params->block_available_up_left) ? int_mem->topLeftPixelCb:128;
	}
	else
	{
		xoff = params->mb_x*8 + (((params->block_num/4)%2)*2 + (params->block_num%4)%2)*4;
		yoff = (((params->block_num/4)/2)*2 + (params->block_num%4)/2 )*4;

		for(i = 0; i<4; ++i)
			topPixels[i] = (params->block_available_up) ? int_mem->topPixelsCr[xoff+i]:128;
		for(i = 0; i<4; ++i)
			leftPixels[i] = (params->block_available_left) ? int_mem->leftPixelsCr[yoff+i]:128;

		*topleftPixel = (params->block_available_up_left) ? int_mem->topLeftPixelCr:128;
	}
}

void store_pixels(blockParams *params, block4x4 currBlock, pixelStore* int_mem)
{
	int i;
	int xoff, yoff;

	if(params->yuv == Y)
	{
		xoff = params->mb_x*16 + (((params->block_num/4)%2)*2 + (params->block_num%4)%2)*4;
		yoff = (((params->block_num/4)/2)*2 + (params->block_num%4)/2 )*4;

		int_mem->topLeftPixelY = int_mem->topPixelsY[xoff+3];
		for(i = 0; i<4; ++i)
		{
			int_mem->topPixelsY[xoff+i] = currBlock[3][i];
			int_mem->leftPixelsY[yoff+i] = currBlock[i][3];
		}
	}
	else if(params->yuv == U)
	{
		xoff = params->mb_x*8 + (((params->block_num/4)%2)*2 + (params->block_num%4)%2)*4;
		yoff = (((params->block_num/4)/2)*2 + (params->block_num%4)/2 )*4;

		int_mem->topLeftPixelCb = int_mem->topPixelsCb[xoff+3];
		for(i = 0; i<4; ++i)
		{
			int_mem->topPixelsCb[xoff+i] = currBlock[3][i];
			int_mem->leftPixelsCb[yoff+i] = currBlock[i][3];
		}
	}
	else
	{
		xoff = params->mb_x*8 + (((params->block_num/4)%2)*2 + (params->block_num%4)%2)*4;
		yoff = (((params->block_num/4)/2)*2 + (params->block_num%4)/2 )*4;

		int_mem->topLeftPixelCr = int_mem->topPixelsCr[xoff+3];
		for(i = 0; i<4; ++i)
		{
			int_mem->topPixelsCr[xoff+i] = currBlock[3][i];
			int_mem->leftPixelsCr[yoff+i] = currBlock[i][3];
		}
	}
}

void intra_pred_4x4_luma(blockParams params,
							int leftBlocks[16],
							int topBlocks[16],
							int topleftBlock,
 									block4x4 output)
{

	int i,j;
	int s0;
	int P_A, P_B, P_C, P_D, P_E, P_F, P_G, P_H, P_I, P_J, P_K, P_L, P_X;

    P_A = topBlocks[0] ; 
    P_B = topBlocks[1] ; 
    P_C = topBlocks[2] ; 
    P_D = topBlocks[3] ; 

  if (params.block_available_up_right)
  {
    P_E = topBlocks[4] ; 
    P_F = topBlocks[5] ; 
    P_G = topBlocks[6] ; 
    P_H = topBlocks[7] ; 
  }
  else
  {
    P_E = P_F = P_G = P_H = P_D;
  }

    P_I = leftBlocks[0]; 
    P_J = leftBlocks[1]; 
    P_K = leftBlocks[2]; 
    P_L = leftBlocks[3]; 

    P_X = topleftBlock;
  
  // prediction
  switch (params.predmode)
  {
  case DC_PRED:                         // DC prediction 
    s0 = 0;
    if (params.block_available_up && params.block_available_left) // no edge
      s0 = (P_A + P_B + P_C + P_D + P_I + P_J + P_K + P_L + 4)/(2*BLOCK_SIZE);
    else if (!params.block_available_up && params.block_available_left) // upper edge
      s0 = (P_I + P_J + P_K + P_L + 2)/BLOCK_SIZE;             
    else if (params.block_available_up && !params.block_available_left) // left edge
      s0 = (P_A + P_B + P_C + P_D + 2)/BLOCK_SIZE;             
    else // top left corner, nothing to predict from
      s0 = 128;                           

    for (j=0; j < BLOCK_SIZE; j++)
      for (i=0; i < BLOCK_SIZE; i++) // store DC prediction
        output[i][j] = s0;
    break;

  case VERT_PRED:                       // vertical prediction from block above 
    for(i=0;i<BLOCK_SIZE;i++)
	{
        output[i][0] = P_A;
        output[i][1] = P_B;
        output[i][2] = P_C;
        output[i][3] = P_D;
	}
	break;

  case HOR_PRED:                        // horizontal prediction from left block 
      for(i=0;i<BLOCK_SIZE;i++)
		{
			output[0][i] = P_I;
			output[1][i] = P_J;
			output[2][i] = P_K;
			output[3][i] = P_L;
		}
    break;

  case DIAG_DOWN_RIGHT_PRED:
    output[0][3] = (P_L + 2*P_K + P_J + 2) / 4; 
    output[0][2] =
    output[1][3] = (P_K + 2*P_J + P_I + 2) / 4; 
    output[0][1] =
    output[1][2] = 
    output[2][3] = (P_J + 2*P_I + P_X + 2) / 4; 
    output[0][0] =
    output[1][1] =
    output[2][2] =
    output[3][3] = (P_I + 2*P_X + P_A + 2) / 4; 
    output[1][0] =
    output[2][1] =
    output[3][2] = (P_X + 2*P_A + P_B + 2) / 4;
    output[2][0] =
    output[3][1] = (P_A + 2*P_B + P_C + 2) / 4;
    output[3][0] = (P_B + 2*P_C + P_D + 2) / 4;
    break;

  case DIAG_DOWN_LEFT_PRED:
    output[0][0] = (P_A + P_C + 2*(P_B) + 2) / 4;
    output[1][0] = 
    output[0][1] = (P_B + P_D + 2*(P_C) + 2) / 4;
    output[2][0] =
    output[1][1] =
    output[0][2] = (P_C + P_E + 2*(P_D) + 2) / 4;
    output[3][0] = 
    output[2][1] = 
    output[1][2] = 
    output[0][3] = (P_D + P_F + 2*(P_E) + 2) / 4;
    output[3][1] = 
    output[2][2] = 
    output[1][3] = (P_E + P_G + 2*(P_F) + 2) / 4;
    output[3][2] = 
    output[2][3] = (P_F + P_H + 2*(P_G) + 2) / 4;
    output[3][3] = (P_G + 3*(P_H) + 2) / 4;
    break;

  case  VERT_RIGHT_PRED:// diagonal prediction -22.5 deg to horizontal plane 
    output[0][0] = 
    output[1][2] = (P_X + P_A + 1) / 2;
    output[1][0] = 
    output[2][2] = (P_A + P_B + 1) / 2;
    output[2][0] = 
    output[3][2] = (P_B + P_C + 1) / 2;
    output[3][0] = (P_C + P_D + 1) / 2;
    output[0][1] = 
    output[1][3] = (P_I + 2*P_X + P_A + 2) / 4;
    output[1][1] = 
    output[2][3] = (P_X + 2*P_A + P_B + 2) / 4;
    output[2][1] = 
    output[3][3] = (P_A + 2*P_B + P_C + 2) / 4;
    output[3][1] = (P_B + 2*P_C + P_D + 2) / 4;
    output[0][2] = (P_X + 2*P_I + P_J + 2) / 4;
    output[0][3] = (P_I + 2*P_J + P_K + 2) / 4;
    break;

  case  VERT_LEFT_PRED:// diagonal prediction -22.5 deg to horizontal plane 
    output[0][0] = (P_A + P_B + 1) / 2;
    output[1][0] = 
    output[0][2] = (P_B + P_C + 1) / 2;
    output[2][0] = 
    output[1][2] = (P_C + P_D + 1) / 2;
    output[3][0] = 
    output[2][2] = (P_D + P_E + 1) / 2;
    output[3][2] = (P_E + P_F + 1) / 2;
    output[0][1] = (P_A + 2*P_B + P_C + 2) / 4;
    output[1][1] = 
    output[0][3] = (P_B + 2*P_C + P_D + 2) / 4;
    output[2][1] = 
    output[1][3] = (P_C + 2*P_D + P_E + 2) / 4;
    output[3][1] = 
    output[2][3] = (P_D + 2*P_E + P_F + 2) / 4;
    output[3][3] = (P_E + 2*P_F + P_G + 2) / 4;
    break;

  case  HOR_UP_PRED:// diagonal prediction -22.5 deg to horizontal plane 
    output[0][0] = (P_I + P_J + 1) / 2;
    output[1][0] = (P_I + 2*P_J + P_K + 2) / 4;
    output[2][0] = 
    output[0][1] = (P_J + P_K + 1) / 2;
    output[3][0] = 
    output[1][1] = (P_J + 2*P_K + P_L + 2) / 4;
    output[2][1] = 
    output[0][2] = (P_K + P_L + 1) / 2;
    output[3][1] = 
    output[1][2] = (P_K + 2*P_L + P_L + 2) / 4;
    output[3][2] = 
    output[1][3] = 
    output[0][3] = 
    output[2][2] = 
    output[2][3] = 
    output[3][3] = P_L;
    break;

  case  HOR_DOWN_PRED:// diagonal prediction -22.5 deg to horizontal plane 
    output[0][0] = 
    output[2][1] = (P_X + P_I + 1) / 2;
    output[1][0] = 
    output[3][1] = (P_I + 2*P_X + P_A + 2) / 4;
    output[2][0] = (P_X + 2*P_A + P_B + 2) / 4;
    output[3][0] = (P_A + 2*P_B + P_C + 2) / 4;
    output[0][1] = 
    output[2][2] = (P_I + P_J + 1) / 2;
    output[1][1] = 
    output[3][2] = (P_X + 2*P_I + P_J + 2) / 4;
    output[0][2] = 
    output[2][3] = (P_J + P_K + 1) / 2;
    output[1][2] = 
    output[3][3] = (P_I + 2*P_J + P_K + 2) / 4;
    output[0][3] = (P_K + P_L + 1) / 2;
    output[1][3] = (P_J + 2*P_K + P_L + 2) / 4;
    break;
  }
}

void intrapred_chroma(blockParams params,
						 int leftPixels[16], 
						 int topPixels[16], 
						 int topleftPixel,
						 block4x4 output)
{
  int i,j;
  int  xoff, yoff;
  
  int ib, ic, iaa;
  
  int  s0, s1;
  int predc;
  
	xoff = (((params.block_num/4)%2)*2 + (params.block_num%4)%2)*4;
	yoff = (((params.block_num/4)/2)*2 + (params.block_num%4)/2 )*4;

  if (HOR_PRED_8 == params.c_ipred_mode)
  {
    for (j=0; j<4; j++)
      for (i=0; i<4; i++)
        output[i][j]=leftPixels[i+yoff];
  }

  if (VERT_PRED_8 == params.c_ipred_mode)
  {
    for (j=0; j<4; j++)
      for (i=0; i<4; i++)
        output[j][i]=topPixels[i+xoff];
  }

  if (PLANE_8 == params.c_ipred_mode)
  {
    // plane prediction
    ib= (34+32)>>6;
    ic= (34+32)>>6;
    iaa= 16*(leftPixels[7] + topPixels[7]);
    
    for (j=0; j<4; j++)
      for (i=0; i<4; i++)
        output[i][j] = max(0,min(128,(iaa+(i+xoff-3)*ib+(j+yoff-3)*ic+16)>>5));
  }

  if (params.c_ipred_mode == DC_PRED_8) // DC prediction
  {
	//===== get prediction value =====
	  s0=s1=0;
	  for (i= 0;i<4;i++) { 
		  if (params.block_available_up)       
			  s0 += topPixels[i];
		  if (params.block_available_left)  
			  s1 += leftPixels[i];
	  }

	  if(params.block_available_left && params.block_available_up)
											predc  = (s0 + s1 + 4)>>3;
	  else if ((params.block_num == 1) && params.block_available_up)              
		  									predc  = (s0   +2) >> 2;
	  else if ((params.block_num == 2) && params.block_available_left)
		  									predc  = (s1   +2) >> 2;
	  else 								 	predc  = 128;

      for (i=0; i<BLOCK_SIZE; i++)
		  for (j=0; j<BLOCK_SIZE; j++)
			  output[i][j]=predc;
	}
}

void intrapred_luma_16x16(blockParams params, 
						  int leftPixels[16],
						  int topPixels[16],
						  int topleftPixel,
						  block4x4 output)       
{
  int s0=0;
  int s1,s2;

  int i,j;

  int ih,iv;
  int ib,ic,iaa;
  int xoff,yoff;

	xoff = (((params.block_num/4)%2)*2 + (params.block_num%4)%2)*4;
	yoff = (((params.block_num/4)/2)*2 + (params.block_num%4)/2 )*4;

  switch (params.i_16_predmode)
  {
  case VERT_PRED_16:                       // vertical prediction from block above
    for(j=0;j<BLOCK_SIZE;j++)
      for(i=0;i<BLOCK_SIZE;i++)
        output[j][i]= topPixels[i+xoff];// store predicted 16x16 block
    break;

  case HOR_PRED_16:                        // horisontal prediction from left block
    for(j=0;j<BLOCK_SIZE;j++)
      for(i=0;i<BLOCK_SIZE;i++)
        output[j][i]= leftPixels[j+yoff]; // store predicted 16x16 block
    break;

  case DC_PRED_16:                         // DC prediction
    s1=s2=0;
    for (i=0; i < MB_BLOCK_SIZE; i++)
    {
      if (params.up_avail)
        s1 += leftPixels[i];    // sum hor pix
      if (params.left_avail)
        s2 += topPixels[i];    // sum vert pix
    }
    
	if (params.up_avail)
		s0 = (params.left_avail) ? (s1+s2+16)>>5 : (s1+8)>>4;   // left edge
	else
    	s0 = (params.left_avail) ?  (s2+8)>>4 : 128;   // top left corner, nothing to predict from

    for(i=0;i<BLOCK_SIZE;i++)
      for(j=0;j<BLOCK_SIZE;j++)
      {
        output[i][j]=s0;
      }
    break;

  case PLANE_16:// 16 bit integer plan pred
    ih=0; iv=0;
    for (i=1;i<8;i++)
    {
        ih += i*(topPixels[7+i] - topPixels[7-i]);
        iv += i*(leftPixels[7+i] - leftPixels[7-i]);
    }
    ih += i*(topPixels[7+i] - topleftPixel);
    iv += i*(leftPixels[7+i] - topleftPixel);

    ib=(5*ih+32)>>6;
    ic=(5*iv+32)>>6;

    iaa=16*(topPixels[15]+leftPixels[15]);

    for (j=0;j< BLOCK_SIZE;j++)
    {
      for (i=0;i< BLOCK_SIZE;i++)
      {
        output[i][j]=max(0,min((iaa+(i+xoff-7)*ib +(j+yoff-7)*ic + 16)>>5, 128));
      }
    }// store plane prediction
    break;
  }
}

void copy_block(blockParams *params, block4x4 curr_block, MacroBlock* mb)
{
		int xoff, yoff, i, j;

	xoff = (((params->block_num/4)%2)*2 + (params->block_num%4)%2)*4;
	yoff = (((params->block_num/4)/2)*2 + (params->block_num%4)/2 )*4;

		if(params->yuv == Y)
		{
			for(i=0;i<4;++i)
				for(j=0;j<4;++j)
					mb->Y[params->block_num][i+yoff][j+xoff] = curr_block[i][j];
		}
		else if(params->yuv == U)
		{
			for(i=0;i<4;++i)
				for(j=0;j<4;++j)
					mb->U[params->block_num][i+yoff][j+xoff] = curr_block[i][j];
		}
		else
		{
			for(i=0;i<4;++i)
				for(j=0;j<4;++j)
					mb->V[params->block_num][i+yoff][j+xoff] = curr_block[i][j];
		}
}

void get_neighbors(blockParams *params)
{
	int tla, tra;
	int left_avail_table[] = {0,1,0,1, 1,1,1,1, 0,1,0,1, 1,1,1,1};
	int top_avail_table[] = {0,0,1,1, 0,0,1,1, 1,1,1,1, 1,1,1,1};
	int top_left_avail_table[] = {4,3,2,1, 3,3,1,1, 2,1,2,1, 1,1,1,1};
	int top_right_avail_table[] = {2,2,1,0, 2,2,1,0, 1,1,1,0, 1,0,1,0};

	tla = top_left_avail_table[params->block_num];
	tra = top_right_avail_table[params->block_num];
	params->block_available_left = left_avail_table[params->block_num] || params->left_avail;
	params->block_available_up = top_avail_table[params->block_num] || params->up_avail;
	params->block_available_up_left = ((tla == 1) || (tla == 2 && params->left_avail) || 
	(tla == 3 && params->up_avail) || 
	(tla == 4 && params->up_avail && params->left_avail)); 
	params->block_available_up_right = ((tra == 1) || (tra == 2 && params->up_avail)); 
}

void intra_prediction(const intraParams *iparams, const MacroBlock *residual_mb, MacroBlock *reconst_mb)
{
	int i;

	int leftPixels[16];
	int topPixels[16];
	int topleftPixel;
	blockParams params;
	static pixelStore int_mem;
	MacroBlock temp;

	params.block_num = 0;
	params.yuv = Y;
	params.mb_x = iparams->mb_x;
	params.i16MB = iparams->i16MB;
	params.c_ipred_mode = iparams->c_ipred_mode;
	params.predmode = iparams->predmode[0];
    params.i_16_predmode = iparams->i_16_predmode;
	params.up_avail = iparams->up_avail; 
	params.left_avail = iparams->left_avail; 
	params.left_up_avail = iparams->left_avail && iparams->up_avail;

	get_neighbors(&params);
	get_pixels(&params, &int_mem, leftPixels, topPixels, &topleftPixel);

	for(i = 0; i<16; ++i)
	{
		params.block_num = i;
		params.predmode = iparams->predmode[i];
		if(params.i16MB)
			intrapred_luma_16x16(params,topPixels, leftPixels, topleftPixel, temp.Y[i]);
		else
		{
			get_neighbors(&params);
			get_pixels(&params, &int_mem, leftPixels, topPixels, &topleftPixel);
			intra_pred_4x4_luma(params,leftPixels, topPixels, topleftPixel, temp.Y[i]);
		}
		reconstruct(params,residual_mb, &temp, reconst_mb);
		store_pixels(&params, reconst_mb->Y[i], &int_mem);
	}	

	params.yuv = U;
	for(i = 0; i<4; ++i)
	{
		params.block_num = i;
		get_neighbors(&params);
		get_pixels(&params, &int_mem, leftPixels, topPixels, &topleftPixel);
		intrapred_chroma(params,topPixels, leftPixels, topleftPixel, temp.U[i]);
		reconstruct(params,residual_mb, &temp, reconst_mb);
		store_pixels(&params, reconst_mb->U[i], &int_mem);
	}
	params.yuv = V;
	for(i = 0; i<4; ++i)
	{
		params.block_num = i;
		get_neighbors(&params);
		get_pixels(&params, &int_mem, leftPixels, topPixels, &topleftPixel);
		intrapred_chroma(params,topPixels, leftPixels, topleftPixel, temp.V[i]);
		reconstruct(params,residual_mb, &temp, reconst_mb);
		store_pixels(&params, reconst_mb->V[i], &int_mem);
	}
	
}

void reconstruct(const blockParams params, const MacroBlock *residual_mb, 
		const MacroBlock *pred_mb, MacroBlock *reconst_mb)
{
	int j,k;
	int x, y;
	if(params.yuv == Y)
	{
		for(j = 0; j < 4; ++j)
			for(k = 0; k < 4; ++k)
			{
				x = pred_mb->Y[params.block_num][j][k];
				y = residual_mb->Y[params.block_num][k][j];
				reconst_mb->Y[params.block_num][j][k] = (x+y) < 0 ? 0 : (((x+y) > 255) ? 255 : (x+y)); 
			}
	}
	else if(params.yuv == U)
	{
		for(j = 0; j < 4; ++j)
			for(k = 0; k < 4; ++k)
			{
				x = pred_mb->U[params.block_num][j][k];
				y = residual_mb->U[params.block_num][j][k];
				reconst_mb->U[params.block_num][j][k] = (x+y) < 0 ? 0 : (((x+y) > 255) ? 255 : (x+y)); 
			}
	}
	else
	{
		for(j = 0; j < 4; ++j)
			for(k = 0; k < 4; ++k)
			{
				x = pred_mb->V[params.block_num][j][k];
				y = residual_mb->V[params.block_num][j][k];
				reconst_mb->V[params.block_num][j][k] = (x+y) < 0 ? 0 : (((x+y) > 255) ? 255 : (x+y)); 
			}
	}
}
