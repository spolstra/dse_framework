/***************************************************************************************
 * 	File: 	cavlc.h
 * 	Description: header file for CAVLC decoder
 * 	Author: Adarsha Rao
 * 	Copyright Information: Morphing Machines Pvt. (Ltd. IISc New Technology Incubation).
 * 						   All rights reserved.
 * 	Last Modified:    09-19-2008
 * 	Revision History: 
 * 		09-19-2008 	Adarsha 	Initial Version
 * ---------------------------------------------------------------------------------
 ***************************************************************************************/

#ifndef CAVLC_H_
#define CAVLC_H_

#define MIN(a, b) ((a < b) ? a : b)

typedef struct transformCoeffType
{
	int lumaDC[16];
	int lumaAC[16][16];
	int chromaDCU[4];
	int chromaDCV[4];
	int chromaACU[4][16];
	int chromaACV[4][16];
}transformCoeffType;

typedef enum CAVLC_TYPE_DEF{
	LumaLevel,
	Intra16x16DCLevel,
	Intra16x16ACLevel,
	ChromaDCLevelU,
	ChromaDCLevelV,
	ChromaACLevelU,
	ChromaACLevelV
} CAVLC_TYPE;

typedef struct cavlcParams{
	unsigned short mb_x;
	unsigned char i16MB;

	unsigned char leftAvail, topAvail; 
	unsigned char leftSkipped, topSkipped;
	unsigned char leftInter, topInter;
	unsigned char cbpLumaLeft, cbpLumaTop, cbpLuma;
	unsigned char cbpChromaLeft, cbpChromaTop, cbpChroma;

	CAVLC_TYPE cavlcType;
	unsigned char blkIdx;
}cavlcParams;

typedef enum YUV_TYPE_DEF{ Y,U,V }YUV_TYPE;

typedef struct NeighborIps{
	unsigned char blkIdx;
	unsigned char blkSize;
	YUV_TYPE yuv;
	unsigned char leftAvailable;
	unsigned char topAvailable;
	unsigned char topRightAvailable;
	unsigned char topLeftAvailable;
}NeighborIps;

typedef struct availInfo{
	unsigned char avail;
	unsigned char mbIdx;
	unsigned char blkIdx;
	YUV_TYPE yuv;
}availInfo;

typedef struct Neighbors{
 	availInfo left;
 	availInfo top;
 	availInfo topRight;
 	availInfo topLeft;
}Neighbors; 

void cavlc(const cavlcParams *cavlcIps1, const nalUnit *nal, const dword *bitPosIn, transformCoeffType *coeffs, dword *bitPosOut);
void getTotalCoeff( char , unsigned char *, unsigned char *, nalUnit *, dword *bitPosIn, dword *bitPosOut);
void calcNc(cavlcParams *, nalUnit *, unsigned char *, unsigned char *,unsigned char *, 
			unsigned char *, dword *bitPosIn, dword *bitPosOut);
unsigned char getTotalZeros( unsigned char , unsigned char, nalUnit* , dword *bitPosIn, dword *bitPosOut);
unsigned char getRunBefore(unsigned char, nalUnit * , dword *bitPosIn, dword *bitPosOut);
unsigned char getLevelPrefix(nalUnit *, dword *bitPosIn, dword *bitPosOut);
void calcLevel(unsigned char , unsigned char , short* , nalUnit*, dword *bitPosIn, dword *bitPosOut);
void calcRun(unsigned char , unsigned char , unsigned short*, nalUnit* , dword *bitPosIn, dword *bitPosOut);
void residualBlockCavlc(cavlcParams *ip, nalUnit *nal, transformCoeffType *coeffs, dword *bitPosIn, dword *bitPosOut);


Neighbors getNeighbors_4x4(NeighborIps x);
availInfo topBlock(availInfo x);
availInfo leftBlock(availInfo x);
availInfo rightBlock(availInfo x);
#endif
