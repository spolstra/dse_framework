#ifndef _INTRA_PREDICTION
#define _INTRA_PREDICTION

#define BLOCK_SIZE 4
#define MB_BLOCK_SIZE 16

typedef struct pixelStore{
	int topPixelsY[1024];
	int topPixelsCb[512];
	int topPixelsCr[512];

	int leftPixelsY[16];
	int leftPixelsCb[8];
	int leftPixelsCr[8];
	
	int topLeftPixelY;
	int topLeftPixelCb;
	int topLeftPixelCr;
}pixelStore;
/*
typedef struct {
	int y[16][16];
	int cb[8][8];
	int cr[8][8];
}MacroBlock;
*/
typedef enum I_16_MODE_DEF
{
  VERT_PRED_16,
  HOR_PRED_16,
  DC_PRED_16,
  PLANE_16 
}I_16_MODE;

typedef enum I_4X4_MODE_DEF
{
  VERT_PRED = 0,
  HOR_PRED,
  DC_PRED,
  DIAG_DOWN_LEFT_PRED,
  DIAG_DOWN_RIGHT_PRED,
  VERT_RIGHT_PRED,
  HOR_DOWN_PRED,
  VERT_LEFT_PRED,
  HOR_UP_PRED
}I_4X4_MODE;

typedef enum I_CHROMA_MODES_DEF
{
  DC_PRED_8 = 0,
  PLANE_8,
  VERT_PRED_8,
  HOR_PRED_8
}I_CHROMA_MODES;

//typedef enum { Y,U,V } YUV_TYPE;

typedef struct intraParams
{
	int mb_x;
	int i16MB;
	int up_avail; 
	int left_avail; 
	I_CHROMA_MODES c_ipred_mode;
	I_4X4_MODE predmode[16];
    I_16_MODE i_16_predmode;
}intraParams;	

typedef struct blockParams
{
	int block_num;
	YUV_TYPE yuv;
	int mb_x;
	int i16MB;
	I_CHROMA_MODES c_ipred_mode;
	I_4X4_MODE predmode;
    I_16_MODE i_16_predmode;
	int up_avail; 
	int left_avail; 
	int left_up_avail;
	int block_available_up; 
	int block_available_left; 
	int block_available_up_left; 
	int block_available_up_right;
}blockParams;	

int min(int a, int b);
int max(int a, int b);
void get_pixels(blockParams *params, pixelStore* int_mem,
		int leftPixels[16], int topPixels[16], int *topleftPixel);
void store_pixels(blockParams *params, block4x4 currBlock, pixelStore* int_mem);
void intra_pred_4x4_luma(blockParams params,
							int leftBlocks[16],
							int topBlocks[16],
							int topleftBlock,
 									block4x4 output);
void intrapred_chroma(blockParams params,
						 int leftPixels[16], 
						 int topPixels[16], 
						 int topleftPixel,
						 block4x4 output);
void intrapred_luma_16x16(blockParams params, 
						  int leftPixels[16],
						  int topPixels[16],
						  int topleftPixel,
						  block4x4 output);       
//void copy_block(blockParams *params, block4x4 curr_block, MacroBlock* mb);
void get_neighbors(blockParams *params);
void tb_intra_pred(blockParams *opParams);
void printMb(MacroBlock *mb);
void intra_prediction(const intraParams *iparams, const MacroBlock *residual_mb, MacroBlock *reconst_mb);
void reconstruct(const blockParams params, const MacroBlock *residual_mb, const MacroBlock *pred_mb, MacroBlock *reconst_mb);
#endif
