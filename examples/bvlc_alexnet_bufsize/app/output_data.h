// File automatically generated by ESPAM

#ifndef output_data_H
#define output_data_H

#include "output_data_Base.h"

class output_data : public output_data_Base {
public:
  output_data(Id n, output_data_Ports *ports);
  virtual ~output_data();

  void main();

  //I/O memory arrays
    float input[1000] = {0}; 
    const int input_len = 1000;

  //const parameters

//node-specific tensor parameters lengths

};
#endif // output_data_H
