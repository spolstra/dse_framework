import unittest
import sys
import os
import logging
import shutil
import multiprocessing as mp

from dseframework import dse, dsehelpers

class TestSobel(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # Need to change to script directory to run testcase
        cwd = os.path.dirname(os.path.abspath(__file__)) + '/'
        os.chdir(cwd)
        logging.basicConfig(level=logging.WARNING)


    def setUp(self):
        # Experiment parameters.
        shutil.copyfile('sobel_map.yml-ref', 'sobel_map.yml')
        self.name = "sobel.yml"
        self.maxlength = sys.maxint
        self.npop = 20
        self.lambda_ = 20
        self.ngen = 4

    def tearDown(self):
        self.project.clean()

    def test_sobel1(self):
        # Random seed and matching reference pareto front.
        seed = 1
        ref_pareto = [(46901524.0, 70.0), (46901554.0, 60.0),
                      (46901554.0, 60.0), (46901554.0, 60.0),
                      (57092888.0, 50.0), (57092888.0, 50.0),
                      (71256618.0, 40.0)]
        self.run_and_compare(seed, ref_pareto)

    def test_sobel2(self):
        # Random seed and matching reference pareto front.
        seed = 2
        ref_pareto = [(46901524.0, 60.0), (57706137.0, 40.0),
                      (88360568.0, 30.0)]
        self.run_and_compare(seed, ref_pareto)

    def test_sobel3(self):
        # Random seed and matching reference pareto front.
        seed = 3
        ref_pareto = [(46901524.0, 60.0), (46901524.0, 60.0),
                      (46901524.0, 60.0), (46901524.0, 60.0),
                      (60810700.0, 40.0)]
        self.run_and_compare(seed, ref_pareto)

    def test_sobel4(self):
        # Random seed and matching reference pareto front.
        seed = 4
        ref_pareto = [(46901524.0, 60.0), (46901564.0, 50.0),
                      (83372498.0, 40.0)]
        self.run_and_compare(seed, ref_pareto)


    def run_and_compare(self, seed, ref_pareto):
        (app, arch, toolbox, project) = dse.setup(self.name, self.maxlength,
                                         seed,
                                         mp.cpu_count())
        self.project = project
        par, logbook = dse.run(toolbox, app, arch, popn=self.npop,
                               ngen=self.ngen, l=self.lambda_)
        logging.info("pareto front: %s" % par)

        self.assertEqual(ref_pareto, par, "pareto: %s" % par)

if __name__ == '__main__':
    # All tests
    #suite = unittest.TestLoader().loadTestsFromTestCase(TestSobel)

    # To select tests
    suite = unittest.TestSuite()
    suite.addTest(TestSobel('test_sobel1'))
    suite.addTest(TestSobel('test_sobel2'))
    suite.addTest(TestSobel('test_sobel3'))
    suite.addTest(TestSobel('test_sobel4'))
    unittest.TextTestRunner(verbosity=2).run(suite)
