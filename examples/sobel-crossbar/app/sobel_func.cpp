#include <stdlib.h>
#include "imageIO.h"
//#include "MemoryMap.h"
#include "sobel_func.h"

void gradient( const int *a1, const int *a2, const int *a3, const int *a4, const int *a5, const int *a6, int *output ){

    *output = ( ((*a4)+((*a5)<<1)+(*a6)) - ((*a1)+((*a2)<<1)+(*a3)) );

}


void absVal( const int *x, const int *y, int *output ) {

    *output = ( (abs(*x)+abs(*y))/4 );

}


void readPixel( int *output ) {
//*
        int pp;
	static FILE *fh = NULL;

	if (fh == NULL) {
           fh = mrOpen("car_gray.B");
	}

	pp = mGetc(fh);
	*output =  pp;
/*/
        static int addr = 0;
        *output = *(ZBT_MEM+(addr++));
//*/
}

void  writePixel( const int *pp ) {
//*
	static FILE *fh = NULL;

	if (fh == NULL) {
           fh = mwOpen("car_gray_sobel.raw");
	}

	mPutc(fh, *pp);
/*/
        static int addr = 0;
        *(ZBT_MEM+(addr++)) = *pp;
//*/
}
