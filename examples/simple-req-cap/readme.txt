Restricting mappings
--------------------
Certain application tasks might not be able to run on certain
architecture processors. To model this the mapping of tasks to
processors has to be restricted.  This is done with a ``operations``
YML property.  The ``operations`` property of an application specifies
a set of operations that are required to run this task.  For the
processor the ``operations`` property specifies the set of operations
that the processor can execute.  A task can now only be mapped onto a
processor if the operations set of the task is a subset of the
operations set of the processor.

DSE with restricted mappings
----------------------------
To demonstrate this requirement-capability restriction we have added a
simple demo project ``examples/simple-req-cap`` to the DSE framework
that has an application model with 6 tasks ``(A,B,C,D0,D1,E)`` mapped
onto an architecture platform with 4 processors ``(X,Y,Y1,Y2)``.

Every task is annotated with execute annotations as we usually do to
model the computational load of a task. But now the execute
annotations of a task are also recorded in the ``operations`` property
of the task. The required operations for each application task are::

    Kahn process A set(['execute3', 'execute2', 'execute1'])
    Kahn process B set(['execute3'])
    Kahn process C set(['execute1'])
    Kahn process D0 set(['execute2', 'execute1'])
    Kahn process D1 set(['execute2', 'execute1'])
    Kahn process E set(['execute1'])

The four processors of the architecture are given the following
capabilities::

    Processor X set(['execute3'])
    Processor Y set(['execute2', 'execute1'])
    Processor Y1 set(['execute1'])
    Processor Y2 set(['execute3', 'execute2', 'execute1'])

We see that task A requires a processor that can perform execute type
1, 2 and 3. So it can only be mapped onto processor Y2.  This set of
requirements on the application side and capabilities on the
architecture side restrict the mappings to the following set of
feasible mappings::

    Kahn process A set([Y2])
    Kahn process B set([Y2, X])
    Kahn process C set([Y1, Y2, Y])
    Kahn process D0 set([Y2, Y])
    Kahn process D1 set([Y2, Y])
    Kahn process E set([Y1, Y2, Y])

When we run a DSE the solutions will come from this restricted set
of valid mappings::

    RunDSE --pop_init 1 -j6 -s0 simple.yml 

This very short DSE gives us the following three mappings::

    Mapping nr: 0
    Kahn process A       ==> Processor Y2           vproc   
    Kahn process B       ==> Processor X            vproc   
    Kahn process C       ==> Processor Y            vproc   
    Kahn process D0      ==> Processor Y            vproc   
    Kahn process D1      ==> Processor Y2           vproc   
    Kahn process E       ==> Processor Y1           vproc   
    Cycles: 4350, Cost: 345
    
    Mapping nr: 1
    Kahn process A       ==> Processor Y2           vproc   
    Kahn process B       ==> Processor Y2           vproc   
    Kahn process C       ==> Processor Y1           vproc   
    Kahn process D0      ==> Processor Y            vproc   
    Kahn process D1      ==> Processor Y2           vproc   
    Kahn process E       ==> Processor Y1           vproc   
    Cycles: 4360, Cost: 235
    
    Mapping nr: 2
    Kahn process A       ==> Processor Y2           vproc   
    Kahn process B       ==> Processor Y2           vproc   
    Kahn process C       ==> Processor Y2           vproc   
    Kahn process D0      ==> Processor Y            vproc   
    Kahn process D1      ==> Processor Y2           vproc   
    Kahn process E       ==> Processor Y            vproc   
    Cycles: 6503, Cost: 160
    
