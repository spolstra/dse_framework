
#include "D.h"

#include <stdlib.h>
#include <iostream>
using namespace std;
#include <unistd.h>

D::D(Id n, D_Ports *ports) : D_Base(n, ports) {}
D::~D() {}

void D::main() {
    int x;

    while (true) {
        ports->in.read(x);
        cout << getFullName() << " received " << x << endl;

        ports->out.write(x);
        execute("execute1");
        execute("execute2");
    }
}
