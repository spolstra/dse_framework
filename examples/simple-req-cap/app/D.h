
#ifndef D_H
#define D_H

#include "D_Base.h"

class D : public D_Base
{
    public:
        D(Id n, D_Ports *ports);
        virtual ~D();

        void main();
};
#endif // D_H
