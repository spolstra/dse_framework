
#include "E.h"

#include <stdlib.h>
#include <iostream>
using namespace std;
#include <unistd.h>

E::E(Id n, E_Ports *ports) : E_Base(n, ports) {}
E::~E() {}

void E::main() {
    int x;

    while (true) {
        ports->in.read(x);
        cout << getFullName() << " received " << x << endl;

        execute("execute1");
    }
}
