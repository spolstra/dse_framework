
#ifndef E_H
#define E_H

#include "E_Base.h"

class E : public E_Base
{
    public:
        E(Id n, E_Ports *ports);
        virtual ~E();

        void main();
};
#endif // E_H
