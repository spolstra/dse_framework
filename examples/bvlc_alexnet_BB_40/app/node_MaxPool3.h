// File automatically generated by ESPAM

#ifndef node_MaxPool3_H
#define node_MaxPool3_H

#include "node_MaxPool3_Base.h"

class node_MaxPool3 : public node_MaxPool3_Base {
public:
  node_MaxPool3(Id n, node_MaxPool3_Ports *ports);
  virtual ~node_MaxPool3();

  void main();
//input array definition
  const int input_dim_0 = 55;
  const int input_dim_1 = 3;
  const int input_dim_2 = 96;
  float input[96][3][55] = {{{0}}};

//output array definition
  const int output_dim_0 = 27;
  const int output_dim_1 = 1;
  const int output_dim_2 = 96;
  float output[96][1][27] = {{{0}}};

//const parameters
const int k_h = 3;
const int k_w = 3;
const int stride = 2;
//specific node parameters and functions
};
#endif // node_MaxPool3_H
