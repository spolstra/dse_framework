// File automatically generated by ESPAM

#include "node_Relu11.h"
#include <stdlib.h>
#include <iostream>
using namespace std;

node_Relu11::node_Relu11(Id n, node_Relu11_Ports *ports) : node_Relu11_Base(n, ports) {}
node_Relu11::~node_Relu11() {}

void node_Relu11::main() {
  // repetition parameters definition
  int q = 13;
  int phase_len = 13;
  int phase; 
 
  // while (1) {
    // loop over the repetitions number
    for (int rep = 0; rep < q ; rep ++) {
      phase = rep % phase_len;

      //reading
      //max tokens port IP0
      int IP0_tokens = 2496;
 
      // read to input
       for ( int t = 0; t < IP0_tokens; t++) {
        ports->IP0.read(input[t]);
      }

      //execution
        for (int n = 0; n < 192; n++) {
          for (int i = 0; i < 2496; i++) {
          execute("ReLU");
        }
      }

      //writing
      //max tokens port OP0
      int OP0_tokens = 2496;
 
      // write to output
       for ( int t = 0; t < OP0_tokens; t++) {
        ports->OP0.write(output[t]);
      }
    }// loop over the phases
  //} while (1)
} // main
