// File automatically generated by ESPAM

#ifndef node_Gemm18_H
#define node_Gemm18_H

#include "node_Gemm18_Base.h"

class node_Gemm18 : public node_Gemm18_Base {
public:
  node_Gemm18(Id n, node_Gemm18_Ports *ports);
  virtual ~node_Gemm18();

  void main();
//input array definition
  const int input_dim_0 = 4096;
  float input[4096] = {0};

//output array definition
  const int output_dim_0 = 4096;
  float output[4096] = {0};

//weights array definition
  const int weights_dim_0 = 4096;
  const int weights_dim_1 = 4096;
  float weights[4096][4096] = {{0}};

//specific node parameters and functions
};
#endif // node_Gemm18_H
