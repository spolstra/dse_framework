// File automatically generated by ESPAM

#ifndef data_0_H
#define data_0_H

#include "data_0_Base.h"

class data_0 : public data_0_Base {
public:
  data_0(Id n, data_0_Ports *ports);
  virtual ~data_0();

  void main();
//output array definition
  const int output_dim_0 = 224;
  const int output_dim_1 = 1;
  const int output_dim_2 = 3;
  const int output_dim_3 = 1;
  float output[1][3][1][224] = {{{{0}}}};

//specific node parameters and functions
};
#endif // data_0_H
