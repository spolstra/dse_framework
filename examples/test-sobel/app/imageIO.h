#ifndef IMAGEIO_H
#define IMAGEIO_H

#include <stdio.h>

//----------------------------------------------
// mrOpen() opens a file for read
//----------------------------------------------
FILE *mrOpen(char *filename);

//-------------------------------------------------
// mwOpen() creates a file for write
//-------------------------------------------------
FILE *mwOpen(char *filename);

//------------------------------------
// mClose() closes a file
//------------------------------------
void mClose(FILE *fh);

//-------------------------------------------------------
//mGetc() gets a character from a file
//-------------------------------------------------------
int mGetc(FILE *fh);

//-------------------------------------------------
//mPutc puts a character to a file
//-------------------------------------------------
void mPutc(FILE *fh, int a);

#endif
