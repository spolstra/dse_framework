
#ifndef C_H
#define C_H

#include "C_Base.h"

class C : public C_Base
{
    public:
        C(Id n, C_Ports *ports);
        virtual ~C();

        void main();
};
#endif // C_H
