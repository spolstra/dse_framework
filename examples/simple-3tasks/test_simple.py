import unittest
import sys
import os
import logging
import shutil
import multiprocessing as mp

from dseframework import dse, dsehelpers

class TestSimple(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # Need to change to script directory to run testcase
        cwd = os.path.dirname(os.path.abspath(__file__)) + '/'
        os.chdir(cwd)
        logging.basicConfig(level=logging.WARNING)


    def setUp(self):
        # Experiment parameters.
        shutil.copyfile('simple_map.yml-ref', 'simple_map.yml')
        self.name = "simple.yml"
        self.maxlength = sys.maxint
        self.npop = 20
        self.lambda_ = 20
        self.ngen = 4

    def tearDown(self):
        self.project.clean()

    def test_simple1(self):
        # Random seed and matching reference pareto front.
        seed = 1
        ref_pareto = [(3010.0, 185.0), (4000.0, 100.0), (8000.0, 75.0)]
        self.run_and_compare(seed, ref_pareto)

    def test_simple2(self):
        # Random seed and matching reference pareto front.
        seed = 2
        ref_pareto = [(3010.0, 185.0), (4000.0, 100.0), (8000.0, 75.0)]
        self.run_and_compare(seed, ref_pareto)

    def test_simple3(self):
        # Random seed and matching reference pareto front.
        seed = 3
        ref_pareto = [(3010.0, 185.0), (4000.0, 100.0)]
        self.run_and_compare(seed, ref_pareto)

    def test_simple4(self):
        # Random seed and matching reference pareto front.
        seed = 4
        ref_pareto = [(3010.0, 185.0), (4000.0, 100.0), (8000.0, 75.0)]
        self.run_and_compare(seed, ref_pareto)


    def run_and_compare(self, seed, ref_pareto):
        (app, arch, toolbox, project) = dse.setup(self.name, self.maxlength,
                                         seed,
                                         mp.cpu_count())
        self.project = project
        par, logbook = dse.run(toolbox, app, arch, popn=self.npop,
                               ngen=self.ngen, l=self.lambda_)
        logging.info("pareto front: %s" % par)
        #print "pareto front: %s" % par

        self.assertEqual(ref_pareto, par, "pareto: %s" % par)

if __name__ == '__main__':
    # All tests
    #suite = unittest.TestLoader().loadTestsFromTestCase(TestSimple)

    # To select tests
    suite = unittest.TestSuite()
    suite.addTest(TestSimple('test_simple1'))
    suite.addTest(TestSimple('test_simple2'))
    suite.addTest(TestSimple('test_simple3'))
    suite.addTest(TestSimple('test_simple4'))
    unittest.TextTestRunner(verbosity=2).run(suite)
