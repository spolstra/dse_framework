import sys
import re
import pickle

# Convert ouput of evalmaps.sh script to pickled tuple list.
# This can be printed together with the pareto front by PrintPareto

# input format:
# mapX
# Elapsed
# Used energy
# empty line

results = []
for line in sys.stdin:
    if "Elapsed time" in line:
        cycles_str = re.findall("Elapsed time: (\d+) units", line)
        energy_str = re.findall("Used energy: (\d+) units", sys.stdin.next())
        cycles = int(cycles_str[0])
        energy = int(energy_str[0])
        print "%d %d" % (cycles, energy)
        results.append((cycles, energy))

print results

with open('dominated.data', 'w') as dom_file:
    pickle.dump(results, dom_file)


