#!/usr/bin/env python

import argparse
import pyyml.ymlmap

# Handle arguments
argparser = argparse.ArgumentParser(description="Mapping resource info")
argparser.add_argument("mapfile", metavar="mapping_yml",
        help = "The mapping yml file")
args = argparser.parse_args()

m = pyyml.ymlmap.parse(args.mapfile)
d = pyyml.ymlmap.to_dict(m)
print "%d processors used" % len(set(d.values()))
