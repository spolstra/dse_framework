#! /usr/bin/env python

import pickle
import argparse
import itertools
import matplotlib.pyplot as plt
import pylab
import sys

# Colors to plot data sets
colors = ['lightblue', 'blue', 'yellow', 'green']
next_color = itertools.cycle(colors).next

# Markers to plot
markers = [u'o', u'v', u'^', u'<', u'>', u's', u'*', 
           u'H', u'D', u'd', r'$\clubsuit$',  r'$\spadesuit$',
    r'$\bigstar$' ]

def plot_pareto(data, label):

    # new plot
    fig, ax = plt.subplots()

    # plot first set of points
    d = data.pop(0)
    color = next_color()
    next_marker = itertools.cycle(markers).next
    for x,y in d:
        ax.scatter(x,y, s=100, facecolors=color, edgecolors='black',
                    marker=next_marker())

    # plot second set of points
    # define new x and y axis, and reset markers and colors
    ax_new = ax.twinx().twiny()
    d = data.pop(0)
    color = next_color()
    next_marker = itertools.cycle(markers).next
    ymin = sys.maxint
    ymax = 0
    for x,y in d:
        ax_new.scatter(x,y, s=100, facecolors=color, edgecolors='black',
                    marker=next_marker())
        if y < ymin: ymin = y
        if y > ymax: ymax = y

    # scale second set of axes
    ax_new.autoscale(True, 'both', True)
    # need to scale y-axis manually
    ax_new.set_ylim([ymin - ymin*0.001, ymax + ymax*0.001])
    
    #plt.xlabel('Execution time (Cycles)')
    #plt.ylabel(label)
    pylab.savefig('pareto-front.png')
    if (args.nograph == False):
        plt.show()

# main
argparser = argparse.ArgumentParser(description="Pareto Printer")
argparser.add_argument('data_sets',  nargs='+', help='Data sets to print')
argparser.add_argument("--energy", "-e", action='store_true',
                       default=False, help="Energy as second objective")
argparser.add_argument("--nograph", "-n", action='store_true',
                       default=False, help="Don't open plot window")
args = argparser.parse_args()

if args.energy:
    label = 'Energy'
else:
    label = 'Cost'

data = []
# Read data sets.
for data_set in args.data_sets:
    with open(data_set) as f:
        if '.data' in data_set:
            data.append(pickle.load(f))
        elif '.txt' in data_set:
            d = []
            for l in f:
                s = l.split()
                d.append((float(s[0]), float(s[1])))
            data.append(d)
                
# Print it.
plot_pareto(data, label)

