/******************************************************************************
*
* Filename: MPG_Decode_L3.h
* Author: Krister Lagerström (krister@kmlager.com)
* Description: This file contains the definitions used by the L3 decoder.
*
******************************************************************************/

#ifndef _MPG_DECODE_L3_H_
#define _MPG_DECODE_L3_H_

#include "MP3_Main.h"

typedef struct {
    FLOAT32 sample[32][18];
    UINT32 init;
} t_hybrid_samples;

typedef struct {
    FLOAT32 v[2][1024];
    UINT32 init;
} t_subband_samples;

typedef struct {
    UINT32 f[576];
} t_data;

/* Global functions and variables (defined here, and used here & elsewhere) */
void MPG_Decode_L3_Init_Song(t_bit_main_data *bitmain);

/* Local functions and variables (defined here, used here) */
void MPG_L3_Requantize(t_channel_packet *chan,
                       const UINT32 *sfreq);

void MPG_Requantize_Process_Long(t_channel_packet *chan,
                                 UINT32 is_pos, UINT32 sfb);

void MPG_Requantize_Process_Short(t_channel_packet *chan, UINT32 is_pos,
                                  UINT32 sfb, UINT32 win);

void MPG_L3_Reorder(t_channel_packet *chan,
                    const UINT32 *sfreq);

void MPG_L3_Stereo(const t_mpeg1_header *header,
                   t_channel_packet *chan_l,
                   t_channel_packet *chan_r);

void MPG_Stereo_Process_Intensity_Long(UINT32 sfreq,
                                       UINT32 sfb,
                                       t_channel_packet *chan_l,
                                       t_channel_packet *chan_r);

void MPG_Stereo_Process_Intensity_Short(UINT32 sfreq,
                                        UINT32 sfb,
                                        t_channel_packet *chan_l,
                                        t_channel_packet *chan_r);

void MPG_L3_Antialias(t_channel_packet *chan);

void MPG_L3_Hybrid_Synthesis(t_channel_packet *chan,
                             t_hybrid_samples *samples);

void MPG_IMDCT_Win(FLOAT32 in[18], FLOAT32 out[36], UINT32 block_type);

void MPG_L3_Frequency_Inversion(t_channel_packet *chan);

void MPG_L3_Subband_Synthesis(const t_mpeg1_header *header,
                              const t_channel_packet *chan_l,
                              const t_channel_packet *chan_r,
                              t_subband_samples *samples,
                              t_data *outdata);

#endif /* _MPG_DECODE_L3_H_ */
