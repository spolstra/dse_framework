#ifndef OWP4_H
#define OWP4_H

#include <stdio.h>
#include <string>
#include "OWP4_Base.h"

class OWP4: public OWP4_Base
{
        std::string mp3dir;

    public:
        OWP4(Id n, OWP4_Ports *ports);
        virtual ~OWP4();

        void main();
};

#endif // OWP4_H
