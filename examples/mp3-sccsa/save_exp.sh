#!/usr/bin/env bash

trap "rm -rf $1; echo "cleanup"; exit 1" INT

# Collect script to copy experiment settings and results.

if [ -z $1 ]; then
    echo "Usage: $0 exp/dir [dse_command]"
    exit 1
fi

# Create experiment directory
if [ ! -e $1 ]; then
    echo "creating $1"
    mkdir -p $1
else
    echo "$1 already exits"
    exit 1
fi

# Save dse command
if [ -n "$2" ]; then
    echo $2 > $1/command.txt
elif [ -f command.txt ]; then
    cp command.txt $1
else
    echo 'no command given'
fi

# Pareto mappings
cp map*.yml logbook.data pareto.data $1

# Copy all evalated mappings
mkdir $1/stored
cp PoolWorker-?/[0-9]*map.yml $1/stored 2> /dev/null
(cd $1 && tar czf stored.tgz stored)

# Hash or copy app/ arch/ files (cpp h pi ps yml)
git rev-parse HEAD > $1/commit_hash
if git diff --exit-code app arch; then
    echo "clean" >> $1/commit_hash
else
    echo "not clean" >> $1/commit_hash
fi

