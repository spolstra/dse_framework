#!/usr/bin/env bash

# GenerateMappings -r -n 1 --nopng app/media_app.yml arch/media_arch.yml
# mv map*yml maps
make
make trace
rm -f results.txt
for i in `ls maps/map_*.yml`; do
    echo $i
    ln -fs $i media_map.yml
    echo $i >> results.txt
    make runarch | egrep 'Elapsed time|Used energy' >> results.txt
    echo >> results.txt
done


