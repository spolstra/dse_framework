import sys
import re
import pickle

# extract objective 1 and 2 from mapping file name.
# input format:
# obj1-obj2-....mapping..._map.yml

results = []
for line in sys.stdin:
        objectives = re.findall("^(\d+)-(\d+)", line)
        obj1, obj2 = objectives[0]
        print "%d %d" % (int(obj1), int(obj2))
        results.append((int(obj1), int(obj2)))

print results

with open('stored.data', 'w') as stored:
    pickle.dump(results, stored)


