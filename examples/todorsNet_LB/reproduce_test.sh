#!/bin/bash

# run_it(counter, n_workers)
function run_it() {
    d=run$1
    make clean
    rm -f todorsNet_LB_map.yml

    # log differs with more processes. end result still the same. at least
    # for small experiments.
    # time RunDSE --nograph --gen 5 --pop 10 -s 1 -j3 todorsNet_LB.yml &> log

    time RunDSE --nograph --gen 5 --pop 10 -s 1 -j $2 todorsNet_LB.yml &> log

    rm -rf $d
    mkdir $d
    cp pareto.data logbook.data $d
    sort log > $d/log
}

echo "compare dse with different sized workerpools"
rm -rf run?

run_it 1 1 
run_it 2 6

echo "diffing.."
diff -r run1 run2
rm -rf run?

echo "compare same sized workerpools"
for i in `seq 1 2`; do
    run_it $i 1
done

echo "diffing.."
diff -r run1 run2


