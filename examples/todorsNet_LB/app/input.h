// File automatically generated by ESPAM

#ifndef input_H
#define input_H

#include "input_Base.h"

class input : public input_Base {
public:
  input(Id n, input_Ports *ports);
  virtual ~input();

  void main();
//output array definition
  const int output_dim_0 = 32;
  const int output_dim_1 = 1;
  float output[1][32] = {{0}};

//specific node parameters and functions
};
#endif // input_H
