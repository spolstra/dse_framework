// File automatically generated by ESPAM

#ifndef SM_H
#define SM_H

#include "SM_Base.h"

class SM : public SM_Base {
public:
  SM(Id n, SM_Ports *ports);
  virtual ~SM();

  void main();
//input array definition
  const int input_dim_0 = 75;
  float input[75] = {0};

//output array definition
  const int output_dim_0 = 2;
  float output[2] = {0};

//weights array definition
  const int weights_dim_0 = 75;
  const int weights_dim_1 = 2;
  float weights[2][75] = {{0}};

//specific node parameters and functions
};
#endif // SM_H
