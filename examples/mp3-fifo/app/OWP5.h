// File automatically generated by ESPAM

#ifndef OWP5_H
#define OWP5_H

#include "OWP5_Base.h"

class OWP5 : public OWP5_Base
{
    public:
        OWP5(Id n, OWP5_Ports *ports);
        virtual ~OWP5();

        void main();
};
#endif // OWP5_H
