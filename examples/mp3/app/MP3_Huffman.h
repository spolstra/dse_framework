#ifndef _MPG_HUFFMAN_H_
#define _MPG_HUFFMAN_H_

extern void MPG_Read_Huffman(const UINT32 part_2_start, const UINT32 gr,
                             const UINT32 ch,
                             const UINT32 sfreq,
                             t_mpeg1_side_info *g_side_info,
                             t_mpeg1_main_data *g_main_data,
                             t_bit_main_data *bitmain);

#endif /* _MPG_HUFFMAN_H_ */
