// File automatically generated by ESPAM

#ifndef Requant2L_H
#define Requant2L_H

#include "Requant2L_Base.h"

class Requant2L : public Requant2L_Base
{
    public:
        Requant2L(Id n, Requant2L_Ports *ports);
        virtual ~Requant2L();

        void main();
};
#endif // Requant2L_H
