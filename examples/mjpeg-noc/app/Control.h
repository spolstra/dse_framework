/*******************************************************************\

		   SESAME project software license

	      Copyright (C) 2005 University of Amsterdam

    This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
   	of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   	     GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
     along with this program; if not, write to the Free Software
      Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
   			   02111-1307, USA.

      For information regarding the SESAME software project see
	      http://sesamesim.sourceforge.net or email
		    jcofflan@users.sourceforge.net

\*******************************************************************/
#ifndef CONTROL_H
#define CONTROL_H

#include "Control_Base.h"

class Control : public Control_Base {
  int ehufco[257], ehufsi[257];
  int lastp, codesize[257], others[257];
  int bits[36], huffval[257], huffsize[257], huffcode[257];
  int LuminanceDCFrequency[257], LuminanceACFrequency[257], frequency[257];
  int ChrominanceDCFrequency[257], ChrominanceACFrequency[257];
  int BitRate;
  int MinThreshold;
  int MaxThreshold;

public:
  Control(Id n, Control_Ports *ports);
  virtual ~Control();

  void MakeHuffman(int *freq);
  void SpecifiedHuffman(int *bts,int *hvls);
  void CodeSize();
  void CountBits();
  void AdjustBits();
  void SortInput();
  void SizeTable();
  void CodeTable();
  void OrderCodes();
  void SetHuffman(int *code, int *size);
  int *ScaleMatrix(int Numerator,int Denominator,int LongFlag,int *Matrix);
  void AddFrequency(int *ptr1,int *ptr2);
  void SetQTableInfo(TTablesInfo *TablesInfo, TQTables *QTab);
  void SetDCHuffTableInfo(TTablesInfo *TablesInfo, int *Bits, int *Val);
  void SetACHuffTableInfo(TTablesInfo *TablesInfo, int *Bits, int *Val);
  void ClearDCFrequency(int *freq);
  void ClearACFrequency(int *freq);

  void main();
};
#endif // CONTROL_H
