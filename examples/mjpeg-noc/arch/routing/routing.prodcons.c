#include <stdio.h>
#include <assert.h>

unsigned int route_packets_c (unsigned int src, unsigned int dest) {
    int routes[2][2] = {
        { 0, 1},
        { 1, 0}
    };

    assert (src < 2);
    assert (dest < 2);
    return routes[src][dest];
}
