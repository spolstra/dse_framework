import sys
import re
import math

if len(sys.argv) != 3:
    print "usage %s <old routing code> <patched code>" % sys.argv[0]
    exit(2)

ERROR_ROUTE = math.pow(2,31) - 1

with open(sys.argv[1], 'r') as incode:
    with open(sys.argv[2], 'w') as outcode:
        for line in incode:
            # default:                return 0;
            patched_line = re.sub(r'(default:\s+return\s+)0;', r'\1 %d;' % ERROR_ROUTE, line)
            outcode.write(patched_line)


